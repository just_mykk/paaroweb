/** @format */

const withLess = require('@zeit/next-less');

module.exports = withLess({
  lessLoaderOptions: {
    javascriptEnabled: true,
  },
  env: {
    BASE_URL_TEST: 'https://7ea968cb312a.ngrok.io',
    BASE_URL_LIVE: 'https://trade.mypaaro.com',
    RAVE_KEY: 'FLWPUBK-77784add5082d13d2ecf9fb60e256bcf-X',
    RAVE_URL: 'https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/hosted/pay',
    RAVE_TEST_KEY: 'FLWPUBK_TEST-0085aee1c220ec16943c4a941a89ea5c-X',
    RAVE_TEST_URL: 'https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/v2/hosted/pay',
    TRUE_LAYER_CLIENT_ID: 'sandbox-mypaaro-c12d53',
    TRUE_LAYER_CLIENT_SECRET: '209eaf82-5203-45cf-872e-8ce1d30ffc43',
  },
});
