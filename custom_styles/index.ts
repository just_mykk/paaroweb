const styles = {
    btnLg: {
        fontSize: '.9rem !important',
        margin: '0 auto',
        display: 'block',
        width: 150
    },
    formInput: {
        backgroundColor: '#FAFAFA',
        fontSize: '.9rem',
        paddingTop: 9,
        paddingBottom: 9
    },
    inputTwo: {
        fontSize: '.9rem',
        paddingTop: 9,
        paddingBottom: 9
    }
}

export default styles
