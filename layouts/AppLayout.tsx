import Head from 'next/head';

const AppLayout = ({pageTitle, children}) => (
    <>
        <Head>
            <title>{pageTitle ? `My Paaro - ${pageTitle}` : 'My Paaro'}</title>
        </Head>
        {children}
    </>
)

export default AppLayout