import AppLayout from "./AppLayout";
import { NextPage } from "next";
import { useRouter } from 'next/router';
import './layout.less';
import { NavBar } from "../components/NavBar";
import { Header } from "../components/Header";

interface Props {
    title?: string
}

const DashboardLayout: NextPage<Props> = (props) => {
    const router = useRouter();

    return (
        <AppLayout pageTitle={props.title}>
            <div id="page-container" className="sidebar-o sidebar-inverse side-scroll page-header-fixed main-content-narrow">
                <NavBar />
                <Header />

                <main id="main-container">
                    <div className="content">
                        {props.children}
                    </div>
                </main>
            </div>

        </AppLayout>
    )
}

export default DashboardLayout