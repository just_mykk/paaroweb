/** @format */

import { useEffect, useState } from 'react';
import { withAuth } from '../hocs/withAuth';
import { WalletService } from '../services/wallet-service';
import Router, { useRouter } from 'next/router';

const PaymentSuccess = () => {
  const router = useRouter();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    updatePayment();
  }, []);

  const updatePayment = async () => {
    const ref: any = router.query.txref;

    if (router.query.cancelled) {
      Router.push('/wallet');
    } else if (router.query.txref) {
      const res = await WalletService.updateWalletFund(ref);
      if (res.respCode === 99) {
        Router.push('/dashboard');
      } else {
        setLoading(false);
      }
    } else {
      Router.push('/dashboard');
    }
  };

  if (loading) {
    return (
      <>
        <div className="loader-container">
          <div className="loader">Loading...</div>
        </div>

        <style>
          {`
            .loader-container {
                height: 100vh;
                display: flex;
                align-items: center;
                justify-content: center;
            }
        `}
        </style>
      </>
    );
  }
  return (
    <>
      <div className="payment">
        <img src="/images/payment_success.svg" alt="success" />
        <p>Your Payment is Successful</p>
        <a href="/dashboard" rel="noreferrer noopener">
          Back to Home
        </a>
      </div>

      <style jsx>{`
        .payment {
          height: 100vh;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        .payment > p {
          font-weight: bold;
          font-size: 1.5rem;
        }

        .payment > img {
          height: 50%;
        }

        .payment > a {
          text-decoration: underline;
        }
      `}</style>
    </>
  );
};

export default withAuth(PaymentSuccess);
