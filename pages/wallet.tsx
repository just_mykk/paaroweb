/** @format */

import DashboardLayout from '../layouts/DashboardLayout';
import { useEffect, useState } from 'react';
import { WalletService } from '../services/wallet-service';
import { GlobalService } from '../services/global-service';
import { Form, Select, Button, InputNumber, Modal, Result } from 'antd';
import styles from '../custom_styles';
import { AppLoader } from '../components/AppLoader';
import { RecipientService } from '../services/recipient-service';
import sharedService from '../services/shared-service';
import { StorageService } from '../services/storage-service';
import { withAuth } from '../hocs/withAuth';

const { Option } = Select;

const Wallet = () => {
  const [wallets, setWallets] = useState([]);
  const [benList, setBenList] = useState([]);
  const [selectedRecipient, setSelectedRecipient] = useState({
    bankId: 0,
    accountName: '',
    accountNumber: '',
    bankSortCode: '',
  });
  const [cashingOut, setCashingOut] = useState(false);
  const [cashoutError, setCashoutError] = useState('');
  const [cashoutSuccess, setCashoutSuccess] = useState('');
  const [paaroBanks, setPaaroBanks] = useState([]);
  const [paymentChannelId, setPaymentChannelId] = useState(0);
  const [funding, setFunding] = useState(false);
  const [fundingSuccess, setFundingSuccess] = useState(false);
  const [loading, setLoading] = useState(true);
  const [fundingError, setFundingError] = useState('');
  const [fundInfo, setFundInfo] = useState<any>({
    reference: '',
    paymentDetails: {
      accountNumber: '',
      accountName: '',
    },
  });
  const [fundAmt, setFundAmt] = useState(0);
  const [fundCrncy, setFundCrncy] = useState('');
  const [fundBank, setFundBank] = useState('');
  const [paymentMethod, setPaymentMethod] = useState('');

  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => {
    setLoading(true);
    const _wallet = await WalletService.getWallets();
    setLoading(false);
    setWallets(_wallet);
  };

  const walletHistory = async (currency: string) => {
    await WalletService.getWalletHistory(currency);
  };

  const onWalletChange = async (val) => {
    const res = await RecipientService.recipientByCurrency(val);
    setBenList(res);
  };

  const onChangeRecipient = (val, opt) => {
    const selected = benList.filter((item) => {
      return item.id === Number(opt.key);
    });
    sharedService.logger('selected recipient', selected[0]);
    setSelectedRecipient(selected[0]);
  };

  const submitCashout = async (form) => {
    setCashoutError('');
    const body = {
      bankId: selectedRecipient.bankId,
      beneficiaryAccount: selectedRecipient.accountNumber,
      beneficiaryName: selectedRecipient.accountName,
      valueAmount: form.amount,
      valueCurrency: form.wallet,
      sortCode: selectedRecipient.bankSortCode,
    };
    sharedService.logger('cashout body', body);

    setCashingOut(true);
    const res = await WalletService.cashout(body);
    setCashingOut(false);

    if (res.error) {
      setCashoutError(res.message);
      sharedService.showNotification('error', res.message, 'Cashout Error');
    }
    if (!res.error) setCashoutSuccess(res.message);
  };

  const cashoutOk = () => {
    setCashoutSuccess('');
    loadData();
  };

  const onFundWalletChange = async (val) => {
    const res = await WalletService.getPaaroBanks(val);
    setPaaroBanks(res);
  };

  const onBankChange = (val, opt) => {
    setPaymentChannelId(Number(opt.key));
  };

  const submitFund = (form) => {
    const body = { ...form, paymentChannelId };
    sharedService.logger('fund', body);
    if (form.paymentMethod === 'BANK_TRANSFER') bankTransfer(body);
    if (form.paymentMethod === 'CARD') cardPayment(body);
  };

  const bankTransfer = async (body: any) => {
    setFunding(true);
    const res = await WalletService.fundWallet(body);
    setFunding(false);

    if (res.respCode === 99) {
      setFundingError(res.respDesc);
      sharedService.showNotification('error', res.respDesc, 'Funding Error');
    }

    if (res.respCode === 0) {
      setFundingSuccess(true);
      setFundInfo(res.body);
      setFundAmt(body.valueAmount);
      setFundCrncy(body.valueCurrency);
      setFundBank(body.bank);
    }
  };

  const cardPayment = async (body: any) => {
    const user = StorageService.getUserDetails();

    setFunding(true);
    const res = await WalletService.fundWallet(body);
    sharedService.logger('card response', res);

    if (res.respCode === 0) {
      if (body.valueCurrency === 'NGN') {
        const data = {
          amount: body.valueAmount,
          currency: body.valueCurrency,
          customer_email: user['email'],
          customer_phone: user['phoneNumber'],
          customer_firstname: user['firstName'],
          customer_lastname: user['lastName'],
          redirect_url: 'https://mypaaro.com/payment-success',
        };
        await WalletService.ravePayment(data, res.body.reference);
      } else {
        // await WalletService.trueLayerPayment(user, )
      }
    } else {
      setFundingError(res.respDesc);
    }

    setFunding(false);
  };

  const fundingOk = () => {
    setFundingSuccess(false);
    loadData();
  };

  const getBgColor = (crncy: string) => {
    let walletClass = 'block block-rounded block-transparent ';
    switch (crncy.toLowerCase()) {
      case 'gbp':
        return walletClass + 'gbp-gradient';
      case 'usd':
        return walletClass + 'usd-gradient';
      default:
        return walletClass + 'linear-gradient';
    }
  };

  return (
    <>
      <div style={{ display: loading ? 'block' : 'none' }}>
        <AppLoader />
      </div>
      <div style={{ display: loading ? 'none' : 'block' }}>
        <DashboardLayout title="Wallet">
          <div className="row gutters-tiny">
            {wallets.map((item) => {
              return (
                <div
                  className="col-md-6 col-xl-4"
                  key={'w' + item.id}
                  onClick={() => walletHistory(item.currency.type)}
                >
                  <a className={getBgColor(item.currency.type)}>
                    <div className="block-content block-content-full block-sticky-options">
                      <span style={{ color: '#fff', fontSize: '.8rem' }}>
                        {item.currency.description}
                      </span>

                      <div className="py-20 text-center">
                        <div className="font-size-h4 font-w700 mb-0 text-white">
                          <span
                            className="font-w600 text-uppercase text-white-op"
                            style={{ fontSize: '.8rem' }}
                          >
                            {item.currency.type}
                          </span>
                          {GlobalService.addComma(item.availableBalance)}
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
              );
            })}
          </div>

          <h2 className="content-heading">Wallet Actions</h2>
          <div className="row">
            <div className="col-md-6">
              <div className="block">
                <div className="block-header block-header-default">
                  <h3 className="block-title">Cashout</h3>
                </div>

                <div className="block-content">
                  <div className="error-div">{cashoutError}</div>
                  <Form layout="vertical" onFinish={submitCashout}>
                    <Form.Item name="wallet" rules={[{ required: true, message: 'Select Wallet' }]}>
                      <Select
                        onChange={onWalletChange}
                        size="large"
                        style={style.formInput}
                        placeholder="Wallet"
                      >
                        {wallets.map((item) => {
                          return (
                            <Option key={'a' + item.id} value={item.currency.type}>
                              {item.currency.type}
                            </Option>
                          );
                        })}
                      </Select>
                    </Form.Item>

                    <Form.Item
                      name="amount"
                      rules={[{ required: true, message: 'Enter Cashout Amount' }]}
                    >
                      <InputNumber
                        placeholder="Amount"
                        style={{ width: '100%', fontSize: '.9rem' }}
                        size="large"
                        formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={(value) => value.replace(/\$\s?|(,*)/g, '')}
                      />
                    </Form.Item>

                    <Form.Item
                      name="recipient"
                      rules={[{ required: true, message: 'Select Recipient' }]}
                    >
                      <Select
                        size="large"
                        style={style.formInput}
                        placeholder="Recipient"
                        showSearch
                        onChange={onChangeRecipient}
                      >
                        {benList.map((item) => {
                          return (
                            <Option key={item.id} value={item.accountName}>
                              {item.accountName} - {item.accountNumber}
                            </Option>
                          );
                        })}
                      </Select>
                    </Form.Item>

                    <Button
                      style={{ ...styles.btnLg, width: '50%', marginBottom: 20 }}
                      shape="round"
                      size="large"
                      type="primary"
                      htmlType="submit"
                      loading={cashingOut}
                    >
                      Submit
                    </Button>
                  </Form>
                </div>
              </div>
            </div>

            <div className="col-md-6">
              <div className="block">
                <div className="block-header block-header-default">
                  <h3 className="block-title">Fund Wallet</h3>
                </div>

                <div className="block-content">
                  <div className="error-div">{fundingError}</div>
                  <Form layout="vertical" onFinish={submitFund}>
                    <Form.Item
                      name="valueCurrency"
                      rules={[{ required: true, message: 'Select Wallet' }]}
                    >
                      <Select
                        onChange={onFundWalletChange}
                        size="large"
                        style={style.formInput}
                        placeholder="Wallet"
                      >
                        {wallets.map((item) => {
                          return (
                            <Option key={'r' + item.id} value={item.currency.type}>
                              {item.currency.type}
                            </Option>
                          );
                        })}
                      </Select>
                    </Form.Item>

                    <Form.Item
                      name="paymentMethod"
                      rules={[{ required: true, message: 'Select Payment Option' }]}
                    >
                      <Select
                        onChange={(val) => setPaymentMethod(val.toString())}
                        size="large"
                        style={style.formInput}
                        placeholder="Payment Option"
                      >
                        <Option value="CARD">Pay Online</Option>
                        <Option value="BANK_TRANSFER">Bank Transfer</Option>
                      </Select>
                    </Form.Item>

                    <Form.Item
                      name="valueAmount"
                      rules={[{ required: true, message: 'Enter Fund Amount' }]}
                    >
                      <InputNumber
                        placeholder="Amount"
                        style={{ width: '100%', fontSize: '.9rem' }}
                        size="large"
                        formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={(value) => value.replace(/\$\s?|(,*)/g, '')}
                      />
                    </Form.Item>

                    {paymentMethod === 'BANK_TRANSFER' ? (
                      <Form.Item
                        name="bank"
                        rules={[{ required: true, message: 'Select Payment Bank' }]}
                      >
                        <Select
                          size="large"
                          style={style.formInput}
                          placeholder="Payment Bank"
                          showSearch
                          onChange={onBankChange}
                        >
                          {paaroBanks.map((item) => {
                            return (
                              <Option key={item.id} value={item.bankName}>
                                {item.bankName}
                              </Option>
                            );
                          })}
                        </Select>
                      </Form.Item>
                    ) : null}

                    <Button
                      style={{ ...styles.btnLg, width: '50%', marginBottom: 20 }}
                      shape="round"
                      size="large"
                      type="primary"
                      htmlType="submit"
                      loading={funding}
                    >
                      Submit
                    </Button>
                  </Form>
                </div>
              </div>
            </div>
          </div>

          <Modal centered visible={cashoutSuccess != ''} footer={null}>
            <Result
              status="success"
              title="Success"
              subTitle={cashoutSuccess}
              extra={[
                <Button onClick={cashoutOk} type="link">
                  Ok
                </Button>,
              ]}
            />
          </Modal>

          <Modal centered visible={fundingSuccess} footer={null} closable={false}>
            <Result
              status="success"
              title="Success"
              subTitle={'Funding Successful'}
              extra={[
                <Button type="primary" shape="round" onClick={fundingOk}>
                  Ok
                </Button>,
              ]}
            >
              <div className="desc">
                <p>
                  <span style={{ fontWeight: 'bold' }}>Transaction Details:</span>
                </p>
                <p style={{ fontSize: '.9rem' }}>
                  Reference Number: <strong>{fundInfo ? fundInfo.reference : ''}</strong>
                </p>
                <p style={{ fontSize: '.9rem' }}>
                  Amount:{' '}
                  <strong>
                    {fundCrncy} {GlobalService.addComma(fundAmt)}
                  </strong>
                </p>
                <p style={{ fontSize: '.9rem' }}>
                  Bank Name: <strong>{fundBank}</strong>
                </p>
                <p style={{ fontSize: '.9rem' }}>
                  Account Number:{' '}
                  <strong>{fundInfo ? fundInfo.paymentDetails.accountNumber : ''}</strong>
                </p>
                <p style={{ fontSize: '.9rem' }}>
                  Account Name:{' '}
                  <strong>{fundInfo ? fundInfo.paymentDetails.accountName : ''}</strong>
                </p>
              </div>
            </Result>
          </Modal>
        </DashboardLayout>
      </div>
    </>
  );
};

const style = {
  formInput: {
    fontSize: '.9rem',
    paddingTop: 9,
    paddingBottom: 9,
  },
};

export default withAuth(Wallet);
