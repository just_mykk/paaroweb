import { Input, Checkbox, Button, Form, Modal, Result } from "antd";
import Link from 'next/link';
import styles from '../custom_styles';
import { useState } from "react";
import OnboardingService from "../services/onboarding-service";
import { withoutAuth } from "../hocs/withoutAuth";

const Register = () => {
    const [checkedTerms, setCheckedTerms] = useState<boolean>(false);
    const [error, setError] = useState<string>('');
    const [success, setSuccess] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);

    const registerUser = async (form) => {
        setError('');

        if (!validFields(form)) return;

        setLoading(true);
        const res = await OnboardingService.registerUser(form);
        setLoading(false);

        if (res.respCode === 99) setError(res.respDesc);
        if (res.respCode === 0) setSuccess(true);
    }

    const validFields = (form): boolean => {
        if (form.password !== form.confirmPassword) {
            setError('Passwords do not match');
            return false;
        }

        if (!checkedTerms) {
            setError('Accept terms and conditions');
            return false;
        }

        return true;
    }

    return (
        <main className="main">
            <a href="/"><img src="images/logo.png" className="main-logo" /></a>

            <p style={{ color: '#fff' }}>Sign Up to Get Started</p>

            <div className="form-div" style={{margin: '0 5px'}}>
                <div className="error-div">{error}</div>

                <Form onFinish={(ev) => registerUser(ev)}>
                    <Form.Item
                        name="firstName"
                        rules={[{ required: true, message: 'First Name is required' }]}
                    >
                        <Input placeholder="First Name" size="large" style={styles.formInput} />
                    </Form.Item>

                    <Form.Item
                        name="lastName"
                        rules={[{ required: true, message: 'Last Name is required' }]}
                    >
                        <Input placeholder="Last Name" size="large" style={styles.formInput} />
                    </Form.Item>

                    <Form.Item
                        name="userName"
                        rules={[{ required: true, message: 'Display Name is required' }]}
                    >
                        <Input placeholder="Display Name" size="large" style={styles.formInput} />
                    </Form.Item>

                    <Form.Item
                        name="phoneNumber"
                        rules={[{ required: true, message: 'Phone Number is required' }]}
                    >
                        <Input type="tel" placeholder="Phone Number" size="large" style={styles.formInput} />
                    </Form.Item>

                    <Form.Item
                        name="email"
                        rules={[{ required: true, message: 'Email is required' }]}
                    >
                        <Input type="email" placeholder="Email" size="large" style={styles.formInput} />
                    </Form.Item>

                    <Form.Item
                        name="password"
                        rules={[{ required: true, message: 'Password is required' }]}
                    >
                        <Input.Password placeholder="Password" size="large" style={styles.formInput} />
                    </Form.Item>

                    <Form.Item
                        name="confirmPassword"
                        rules={[{ required: true, message: 'Confirm Password is required' }]}
                    >
                        <Input.Password placeholder="Confirm Password" size="large" style={styles.formInput} />
                    </Form.Item>

                    <div className="checkbox-row">
                        <Checkbox style={{ fontSize: '.7rem' }} onChange={(ev) => setCheckedTerms(ev.target.checked)}>
                            I agree to the <a href="/terms"><span className="bold-span">Terms & Conditions </span></a>and <a href="/privacy"><span className="bold-span">Privacy Policy</span></a>
                        </Checkbox>
                    </div>

                    <Button
                        style={styles.btnLg}
                        shape="round"
                        size="large"
                        type="primary"
                        htmlType="submit"
                        loading={loading}
                    >Sign Up</Button>
                </Form>
            </div>


            <a href="/login" className="new-user" style={{ marginBottom: 30 }}>
                <span style={{ opacity: .7 }}>Registered Already? </span> <strong>Sign In</strong>
            </a>

            <Modal
                centered
                visible={success}
                footer={null}
                closable={false}
            >
                <Result
                    status='success'
                    title='Registration Successful'
                    subTitle={'Check your email to confirm your account'}
                    extra={[<a href="/login"><Button type="link">Goto Sign In</Button></a>]}
                />
            </Modal>

            <style jsx>
                {`
                    .main {
                        min-height: 100vh;
                        background-color: #126637;
                        display: flex;
                        flex-direction: column;
                        justify-content: center;
                        align-items: center;
                    }
                    .form-div {
                        background-color: #fff;
                        border-radius: 10px;
                        padding: 40px 30px;
                        min-width: 350px;
                    }
                    .checkbox-row {
                        display: flex;
                        justify-content: space-between;
                        align-items: center;
                        margin-bottom: 30px;
                    }
                    .forgot-password {
                        font-size: .7rem;
                        font-weight: bold;
                        color: inherit;
                    }
                    .new-user {
                        color: #fff;
                        margin-top: 30px;
                        font-size: .7rem;
                    }
                    a {
                        margin-right: 0;
                    }
                    
                `}
            </style>
        </main>
    )
}

export default withoutAuth(Register)