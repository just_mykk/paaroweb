import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Axios from 'axios';
import sharedService from '../../services/shared-service';

const Activate = () => {
    const router = useRouter();
    const { email, activationCode } = router.query;
    const [activating, setActivating] = useState(false);
    const [message, setMessage] = useState('');

    useEffect(() => {
        activateEmail();
    }, []);

    const activateEmail = async () => {
        setActivating(true);
        try {
            const response = await Axios(`https://trade.mypaaro.com/user/activate?code=${activationCode}&email=${email}`, {headers: {'Content-Type': 'application/json'}});
            // const response = await Axios(`https://mypaaro.herokuapp.com/user/activate?code=${activationCode}&email=${email}`, { headers: { 'Content-Type': 'application/json' } });
            setActivating(false);
            setMessage(response.data.respDesc);
        } catch (error) {
            setActivating(false);
            sharedService.logger('activate error', error.response);
            setMessage(error.response ? error.response.data.respDesc : 'Account activation failed');
        }
    };

    return (
        <h1>{activating ? 'ACTIVATING' : message}</h1>
    )
};

Activate.getInitialProps = (ctx) => {
    return {};
};

export default Activate
