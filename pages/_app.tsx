import React, {useEffect} from 'react';
import { AppProps } from 'next/app';
import Head from 'next/head';
import '../styles/index.less';
import { GoogleReCaptchaProvider, GoogleReCaptcha } from 'react-google-recaptcha-v3';
import sharedService from '../services/shared-service';

const MyApp = ({ Component, pageProps }: AppProps) => {
    useEffect(() => {
        const script = document.createElement('script');
        script.src = 'js/pages/be_tables_datatables.js';
        document.body.append(script);
    }, []);

    return (
        <>
            <Head>
                <title>My Paaro</title>
            </Head>

            <GoogleReCaptchaProvider reCaptchaKey="6LcHyOYUAAAAAFQ-hTd4ZTKHNNI20wyTfNLgWFB4">
                <GoogleReCaptcha onVerify={(token) => sharedService.logger('recaptcha token', token)}/>
                <Component {...pageProps} />
            </GoogleReCaptchaProvider>
        </>
    )
}

export default MyApp
