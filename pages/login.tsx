import { Input, Checkbox, Button, Form } from "antd";
import styles from '../custom_styles';
import OnboardingService from "../services/onboarding-service";
import { useState } from "react";
import { withoutAuth } from "../hocs/withoutAuth";
import { StorageService } from "../services/storage-service";
import Router from "next/router";

const Login = () => {
    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<string>('');

    const submitForm = async (ev) => {
        setLoading(true);
        setError('');

        const res = await OnboardingService.userLogin(ev.email, ev.password);
        setLoading(false);

        if (res) setError(res);
        if (!res) {
            const savedFund = StorageService.getSavedFund();
            const goto = localStorage.getItem('@goto');

            const pageToLoad = goto ? goto : savedFund ? '/request' : '/dashboard';
            // window.open(savedFund ? '/request' : '/dashboard', '_self');
            Router.push(pageToLoad);
        }
    }

    return (
        <main className="main">
            <a href="/"><img src="images/logo.png" className="main-logo" /></a>

            <p style={{ color: '#fff' }}>Sign In to Access your Account</p>

            <div className="form-div">
                <div className="error-div">{error}</div>

                <Form onFinish={submitForm}>
                    <Form.Item
                        name="email"
                        rules={[{ required: true, message: 'Enter your email' }]}
                    >
                        <Input placeholder="Email Address" type="email" size="large" style={styles.formInput} />
                    </Form.Item>

                    <Form.Item
                        name="password"
                        rules={[{ required: true, message: 'Enter your password' }]}
                    >
                        <Input.Password placeholder="Password" size="large" style={styles.formInput} />
                    </Form.Item>

                    <div className="checkbox-row">
                        <div></div>
                        <a href="/reset-password" className="forgot-password">Forgot Password?</a>
                    </div>

                    <Button
                        style={styles.btnLg}
                        shape="round"
                        size="large"
                        type="primary"
                        htmlType="submit"
                        loading={loading}
                    >
                        Sign In
                    </Button>
                </Form>
            </div>


            <a href="/register" className="new">
                <span style={{ opacity: .7 }}>Not yet registered? Get Started</span> <strong>Here</strong>
            </a>

            <style jsx>
                {`
                    .main {
                        min-height: 100vh;
                        background-color: #126637;
                        display: flex;
                        flex-direction: column;
                        justify-content: center;
                        align-items: center;
                    }
                    .form-div {
                        background-color: #fff;
                        border-radius: 10px;
                        padding: 40px 30px;
                        min-width: 350px;
                    }
                    .checkbox-row {
                        display: flex;
                        justify-content: space-between;
                        align-items: center;
                        margin-bottom: 30px;
                    }
                    .forgot-password {
                        font-size: .7rem;
                        font-weight: bold;
                        color: inherit;
                    }
                    .new {
                        color: #fff;
                        margin-top: 30px;
                        font-size: .7rem;
                    }
                `}
            </style>
        </main>
    )
}

export default withoutAuth(Login)