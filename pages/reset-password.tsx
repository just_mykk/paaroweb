import { Input, Button, Form, Modal, Result } from "antd";
import Link from 'next/link';
import styles from '../custom_styles';
import OnboardingService from "../services/onboarding-service";
import { useState } from "react";
import { withoutAuth } from "../hocs/withoutAuth";

const ResetPassword = () => {
    const [success, setSuccess] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<string>('');

    const submit = async (ev) => {
        setError('');
        setLoading(true);
        const res = await OnboardingService.resetPassword(ev.email);
        setLoading(false);

        if (res) setError(res);
        if (!res) setSuccess(true);
    }

    return (
        <main className="main">
            <a href="/"><img src="images/logo.png" className="main-logo" /></a>

            <p style={{ color: '#fff', textAlign: 'center' }}>
                Enter Registered Email Address <br />to Retrieve Your Password
            </p>

            <div className="form-div">
                <div className="error-div" style={{ color: 'red' }}>{error}</div>

                <Form onFinish={(ev) => submit(ev)}>
                    <Form.Item
                        name="email"
                        rules={[{ required: true, message: 'Enter your email' }]}
                    >
                        <Input placeholder="Email Address" size="large" style={styles.formInput} />
                    </Form.Item>

                    <Button loading={loading} style={{ ...styles.btnLg, width: 'auto' }} shape="round" size="large" type="primary" htmlType="submit">
                        Retrieve Password
                    </Button>
                </Form>
            </div>

            <a href="/login" className="new">
                <span style={{ opacity: .7 }}>Go Back to</span> <strong>Sign In</strong>
            </a>

            <Modal
                centered
                visible={success}
                footer={null}
                onCancel={() => setSuccess(false)}
            >
                <Result
                    status='success'
                    title='Success'
                    subTitle={'Please check your email'}
                    extra={[<a href="/login"><Button type="link">Goto Sign In</Button></a>]}
                />
            </Modal>

            <style jsx>
                {`
                    .main {
                        min-height: 100vh;
                        background-color: #126637;
                        display: flex;
                        flex-direction: column;
                        justify-content: center;
                        align-items: center;
                    }
                    .form-div {
                        background-color: #fff;
                        border-radius: 10px;
                        padding: 40px 30px;
                        min-width: 350px;
                        margin-bottom: 30px;
                    },
                    .new {
                        color: #fff;
                        font-size: .7rem;
                    }
                `}
            </style>
        </main>
    )
}

export default withoutAuth(ResetPassword)