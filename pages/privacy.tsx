import DashboardLayout from "../layouts/DashboardLayout";

export default () => (
    <DashboardLayout>
        <h1 className="content-heading">PRIVACY POLICY</h1>

        <p>
            This Privacy Policy explains what you can expect from us and what we need from you in relation to your personal data. Please read this carefully as this Privacy Policy is legally binding when you use our Services.
        </p>
        <p>By continuing your interactions with us, such as by submitting information to us, or using our Services, you confirm that you understand and consent to the collection, use, disclosure, and processing of your personal data (or the personal data of any individual you provide) in the manner as set forth in this Privacy Policy. Check the User Agreement for the meaning of defined words (those with capital letters).</p>
        <p>For the purpose of the relevant data protection regulations, the company in charge of your information is My Paaro. Ltd. (RC                  ), a company incorporated under the laws of the United Kingdom, with its address at [                                                                ].</p>

        <h3>How do we protect your personal data</h3>
        <p>We are serious about guarding the security of your personal data and use a secure server to store your personal data. All information you provide to us is stored on our secure servers. Any payment transactions will be encrypted using Transport Layer Security technology. Where we have given you (or where you have chosen) a password which enables you to access certain parts of our Services, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.</p>
        <p>As you will know, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, as we cannot guarantee the security of your data during transmission, any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorized access.</p>
        <p>We restrict access of your personal data to those employees of My Paaro who have a business reason for knowing such information. We continuously educate and train our employees about the importance of confidentiality and privacy of customer information. We maintain physical, electronic and procedural safeguards that comply with applicable laws and regulations (including, without limitation, the Personal Data Protection Act 2012 (No. 26 of 2012)) to protect your personal data from unauthorised access.</p>
        <h5>Information we may collect from you</h5>
        <p>We may collect and use the following data about you.</p>
        <h5>Information you give us.</h5>
        <p>You may give us information about you by filling in forms on our Mobile APP, Website or via face-to-face meetings and/or correspondence with us, e.g. email. This includes information you provide when you register to use our Services, participate in discussion boards or other social media functions on our Website, enter a competition, promotion or survey, and when you report a problem through our various communication channels. The information you give us may include your name, address, email address, phone number, financial information (including credit card, debit card, or bank account information), payment reason, geographical location, social security number, national insurance number, Biometric Verification Number (BVN), personal description and photograph.</p>
        <p>We may also need additional commercial and/or identification information from you,, e.g. if you send or receive certain high-value or high volume transactions, or as needed to comply with our obligations under applicable laws and regulations, including but not limited to our anti-money laundering obligations.</p>
        <p>In providing the personal data of any individual (other than yourself) to us during your use of our Services, you warrant that you have obtained consent from such individual to disclose his/her personal data to us, as well his/her consent to our collection, use and disclosure of such personal data, for the purposes set out in this Privacy Policy.</p>

        <h5>Information we collect about you.</h5>
        <p>With regard to each of your visits to our Website or Mobile APP, we may automatically collect the following information:</p>
        <ul>
            <li>details of the transactions you carry out or when using our Services, including geographic location from which the transaction originates;</li>
            <li>technical information, including the Internet Protocol (IP) address used to connect your computer to the Internet, your login information, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform;</li>
            <li>information about your visit, including the full Uniform Resource Locators (URL) clickstream to, through and from our Website (including date and time); products you viewed or searched for; page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), and methods used to browse away from the page and any phone number used to call our Customer Support number.</li>
        </ul>

        <h5>Information we receive from other sources. </h5>
        <p>We may receive information about you if you use any of the channels we operate or the other services we provide. We are also working closely with third parties and may receive information about you from them for example, banks and payment service providers you use to transfer money to us; banks and payment service providers of your recipient; business partners; sub-contractors in technical, payment and delivery services; advertising networks; analytics providers; search information providers; and credit reference agencies.</p>

        <h5>Information from social media networks.</h5>
        <p>If you log in to our Services using your social media account (for example, Facebook or Google) we will receive relevant information that is necessary to enable our Services and authenticate you. The social media network will provide us with access to certain information that you have provided to them, including your name, profile image and e-mail address. We use such information, together with any other information you directly provide to us when registering or using our Services, to create your account and to communicate with you about the information, products and services that you request from us. You may also be able to specifically request that we have access to the contacts in your social media account so that you can send a referral link to your family and friends. We will use, disclose and store all of this information in accordance with this privacy policy.</p>

        <h3>Cookies</h3>
        <p>Our Website uses cookies, some of which may collect your personal data, to distinguish you from other users of our Website. This helps us to provide you with a good experience and also allows us to improve our Website. For detailed information on the cookies we use and the purposes for which we use them see our Cookie Policy.</p>

        <h3>Uses made of the information</h3>
        <p>We use your information in the following ways:</p>
        <ul>
            <li>to carry out our obligations relating to your contracts with us and to provide you with the information, products and services that you request from us;</li>
            <li>to comply with any applicable legal and/or regulatory requirements;</li>
            <li>to notify you about changes to our Services;</li>
            <li>as part of our efforts to keep our Services safe and secure;</li>
            <li>to administer our Website, Mobile APP and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes;</li>
            <li>to improve our Website and Mobile APP and to ensure other Services are presented in the most effective manner;</li>
            <li>to measure or understand the effectiveness of advertising we serve and to deliver relevant advertising to you;</li>
            <li>to allow you to participate in interactive features of our Services, when you choose to do so;</li>
            <li>to provide you with information about other similar goods and services we offer;</li>
            <li>to provide you, or permit selected third parties to provide you, with information about goods or services we feel may interest you; or</li>
            <li>combine information we receive from other sources with the information you give to us and information we collect about you. We may use this information and the combined information for the purposes set out above (depending on the types of information we receive).</li>
        </ul>

        <h3>Disclosure of your information</h3>
        <p>We may share your information with selected third parties including:</p>
        <ul>
            <li>affiliates, business partners, suppliers and sub-contractors for the performance of any contract we enter into with them or you;</li>
            <li>advertisers and advertising networks solely to select and serve relevant adverts to you and others; and</li>
            <li>analytics and search engine providers that assist us in the improvement and optimization of our site.</li>
        </ul>
        <p>We may disclose your personal data to third parties:</p>
        <ul>
            <li>in the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets;</li>
            <li>if we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce or apply our User Agreement and other applicable agreements; or to protect the rights, property, or safety of My Paaro, our customers, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction;</li>
            <li>to assist us in conducting or co-operating in investigations or proceedings relating to fraud or other illegal activity where we believe it is reasonable and appropriate to do so;</li>
            <li>to prevent and/or detect fraud or crime;</li>
            <li>in response to a subpoena, warrant, court order, order or notice of any government authority, public agency, or law enforcement agency, or as otherwise required by law;</li>
            <li>to assess financial and insurance risks;</li>
            <li>to recover debt or in relation to your insolvency; and</li>
            <li>to develop customer relationships, services and systems.</li>
        </ul>

        <h3>Accuracy of your personal data</h3>
        <p>In order to ensure that your personal data is current, complete and accurate, please update us if there are changes to your personal data by contacting us at <a href="mailto:privacy@mypaaro.com">privacy@mypaaro.com</a>.</p>

        <h3>Where we transfer and store your personal data</h3>
        <p>The personal data that we collect from you may be transferred to, and stored at, a destination outside the United Kingdom. It may also be processed by staff operating outside the United Kingdom, who work for us or for one of our suppliers. Such staff may be engaged in, among other things, the fulfilment of your payment order, the processing of your payment details and the provision of support services. By submitting your personal data to us, you agree to such transfer, storage and processing of your personal data. We will take all steps reasonably necessary to ensure that your personal data is transferred and treated securely and in accordance with this Privacy Policy and the applicable laws and regulations.</p>
        <p>We will cease to retain documents containing your personal data,or remove the means by which such personal can be associated with you, as soon as it is reasonable to assume that the retention of such personal data no longer serves the purposes for which it as collected; and such retention is no longer necessary for legal or business purposes.</p>
        <p>Your rights<br />On giving reasonable notice to us, you have the right to ask us to stop collecting, using, disclosing and/or otherwise processing your personal data for any or all purposes, by contacting us at <a href="mailto:privacy@mypaaro.com">privacy@mypaaro.com</a>, and indicating the specific purposes for which you would like us to cease collecting, using, disclosing and/or otherwise processing your personal data.</p>
        <p>Within 10 Business Days from our receipt of your request, we will notify you of the consequences of our acceding to the same. Please note that, depending on the nature and scope of your request, we may not be in a position to continue providing our products or services to you. If you do not respond to our notice within 10 Business Days of such notice (e.g., by confirming or retracting your request), we will proceed to process your request.</p>
        <p>Notwithstanding any such request, we may be entitled to continue retaining your personal data in accordance with applicable laws and regulations.</p>

        <h3>Third party websites</h3>
        <p>Our Website may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility for them. Please check these policies before you submit any personal data to these websites.</p>

        <h3>Access to and correction of personal data</h3>
        <p>Subject to applicable laws and regulations, you may have the right to request for access to your personal data that is in our possession or under our control, as well as information about the ways in which such personal data has been or may have been used or disclosed by us. Your right of access can be exercised in accordance with applicable laws and regulations. Any access request may be subject to a fee to meet our costs in providing you with details of the information we hold about you.</p>
        <p>Subject to applicable laws and regulations, you may also have the right to request for us to correct your personal data that is in our possession or under our control. For the avoidance of doubt, this right does not affect your obligations under the User Agreement to provide complete, accurate and truthful information to us during the signup process, and to ensure that the information recorded on your  Account is accurate and up to date at all times.</p>

        <h3>Changes to our privacy policy</h3>
        <p>We may change this Privacy Policy at any time by posting a revised version on this page. The revised version will be effective once it is posted. Where appropriate, we may notify you of any changes to this Privacy Policy by email. Please check back frequently to see if there are any updates or changes to our Privacy Policy, as it is legally binding on you.</p>

        <h3>Contact</h3>
        <p>Questions, comments, complaints, and requests regarding this Privacy Policy are welcomed and should be addressed to <a href="mailto:privacy@mypaaro.com">privacy@mypaaro.com</a></p>

        <style jsx>
            {`
                h3, h5 {
                    font-family: Montserrat;
                }

                h3 {
                    font-weight: bold;
                }

                h5 {
                    font-weight: 600;
                }
            `}
        </style>
    </DashboardLayout>
)