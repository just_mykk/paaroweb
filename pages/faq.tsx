import { useEffect, useState } from "react";
import { GlobalService } from "../services/global-service";
import DashboardLayout from "../layouts/DashboardLayout";
import { AppLoader } from "../components/AppLoader";


export default () => {
    const [faqList, setFaqList] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        loadData();
    }, []);

    const loadData = async () => {
        const res = await GlobalService.getFaqs();
        setLoading(false);
        setFaqList(res);
    }

    if (loading) return <AppLoader/>

    return (
        <DashboardLayout title="FAQ">
            <h2 className="content-heading">Frequently Asked Questions</h2>

            {
                faqList.map((item, index) => {
                    return (
                        <div className="block" key={index.toString()}>
                            <div className="block-header block-header-default">
                                <h3 className="block-title">
                                    <strong>{index + 1}.</strong> {item.name}
                                </h3>
                                <div className="block-options">
                                    <button type="button" className="btn-block-option">
                                        <i className="si si-question" />
                                    </button>
                                </div>
                            </div>
                            {
                                item.faqs.map((val, i) => {
                                    return (
                                        <div key={`${i}faq`} className="block-content block-content-full">
                                            <div id={`faq_${i}`} role="tablist" aria-multiselectable="true">
                                                <div className="block block-bordered block-rounded mb-5">
                                                    <div className="block-header" role="tab" id="faq1_h1">
                                                        <a className="font-w600 text-body-color-dark" data-toggle="collapse" data-parent={`#faq_${i}`} href={`#q_${i}`} aria-expanded="true" aria-controls={`faq_q_${i}`}>
                                                            {index + 1}.{i + 1} {val.question}
                                                        </a>
                                                    </div>
                                                    <div id={`q_${i}`} className="collapse" role="tabpanel" aria-labelledby={`faq_q_${i}`}>
                                                        <div className="block-content border-t">
                                                            <p>{val.answer}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    )
                })
            }

            <style jsx>
                {`
                    .block-content {
                        padding: 5px 18px 1px 18px;
                    }
                `}
            </style>
        </DashboardLayout>
    )
}