import DashboardLayout from "../layouts/DashboardLayout"
import { useEffect, useState } from "react"
import { StorageService } from "../services/storage-service";
import OnboardingService from "../services/onboarding-service";
import { GlobalService } from "../services/global-service";
import { AppLoader } from "../components/AppLoader";
import { ProfileService } from "../services/profile-service";
import sharedService from "../services/shared-service";
import { Form, Input, Button, Modal, Spin } from "antd";
import styles from "../custom_styles";
import { CloudUploadOutlined, CheckCircleOutlined, ClockCircleOutlined, CloseCircleOutlined } from "@ant-design/icons";
import { withRouter } from "next/router";

const Profile = () => {
    const [loading, setLoading] = useState(false);
    const [user, setUser] = useState<any>({});
    const [docArray, setDocArray] = useState([]);
    const [changing, setChanging] = useState(false);
    const [bvnModal, setBvnModal] = useState(false);
    const [uploadingBvn, setUploadingBvn] = useState(false);
    const [bvnCurrency, setBvnCurrency] = useState('');
    const [profilePic, setProfilePic] = useState('');
    const [uploading, setUploading] = useState(false);

    useEffect(() => {
        loadApp();
        loadDoc();
    }, []);

    const loadApp = async () => {
        let _user = StorageService.getUserDetails();
        if (!_user) _user = await OnboardingService.getUser();
        setUser(_user);
        setProfilePic(_user.imageUrl);
    };

    const loadDoc = async () => {
        setLoading(true);
        const res = await GlobalService.getCurrencies();
        let _array = [];

        for (let item of res) {
            const response = await ProfileService.getKycList(item.type);
            _array.push({ currency: item.type, ...response });
        }
        sharedService.logger('doc', _array);
        setDocArray(_array);

        setLoading(false);
    }

    const changePassword = async (form) => {
        setChanging(true);
        const res = await ProfileService.changePassword(form);
        setChanging(false);

        if (res) sharedService.showNotification('error', res, 'Change Password Error');
        if (!res) sharedService.showNotification('success', 'Password Changed Successfully', 'Success');
    }

    const statusIcon = (status: string) => {
        switch (status) {
            case 'VERIFIED':
                return <CheckCircleOutlined style={{ color: 'green', fontSize: 60 }} />;
            case 'PENDING_VERIFICATION':
                return <ClockCircleOutlined style={{ color: '#F1C232', fontSize: 60 }} />
            case 'DECLINED':
                return <CloseCircleOutlined style={{ color: 'red', fontSize: 60 }} />
            default:
                return <CloudUploadOutlined style={{ fontSize: 60, color: 'red' }} />;
        }
    }

    const upload = (type: string, item: any, status: string) => {
        if (status !== 'VERIFIED') {
            const imageFile = document.getElementById('imageFile');
            imageFile.click();

            imageFile.onchange = (ev) => {
                //@ts-ignore
                const file = ev.target.files[0];

                const reader = new FileReader();
                reader.addEventListener('load', () => {
                    let body = {};
                    sharedService.logger('res', reader.result);
                    if (type === 'profile') {
                        uploadProfile(reader.result.toString());
                        setProfilePic(reader.result.toString());
                    } else {
                        body = { currencyType: item.currency, base64file: reader.result };
                        uploadToServer(body, type);
                    }
                });
                reader.readAsDataURL(file);
            }
        }
    }

    const uploadProfile = async (val: string) => {
        const res = await ProfileService.uploadProfilePic({ base64Image: val });
        if (res) {
            sharedService.showNotification('error', res, 'Upload Error');
            setProfilePic(user.imageUrl);
        }

        if (!res) {
            sharedService.showNotification('success', 'Profile Picture Uploaded Successfully', 'Success');
            const profile = await OnboardingService.getUser();
            if (profile) {
                StorageService.saveUserDetails(profile);
                location.reload();
            }
        }
    }

    const uploadToServer = async (body: any, type: string) => {
        let res;
        setUploading(true);
        if (type === 'passport') res = await ProfileService.uploadPassport(body);
        if (type === 'validId') res = await ProfileService.uploadValidId(body);
        if (type === 'bill') res = await ProfileService.uploadBill(body);
        setUploading(false);
        handleResponse(res);
    }

    const submitBvn = async (form) => {
        const body = { ...form, currencyType: bvnCurrency };
        setUploadingBvn(true);
        const res = await ProfileService.uploadBvn(body);
        setUploadingBvn(false);
        setBvnModal(false);
        handleResponse(res);
    }

    const handleResponse = (res) => {
        if (res) sharedService.showNotification('error', res, 'Upload Error');
        if (!res) {
            sharedService.showNotification('success', 'Upload Successful', 'Success');
            loadDoc();
            //@ts-ignore
            document.getElementById('imageFile').value = '';
        }
    }

    const onBvnClick = (crncy: string, status: string) => {
        if (status !== 'VERIFIED')
            setBvnModal(true);
        setBvnCurrency(crncy);
    }

    if (loading) return <AppLoader/>

    return (
        <DashboardLayout title="Profile">
            <div className="bg-image bg-image-bottom linear-gradient">
                <div className="bg-primary-dark-op py-30">
                    <div className="content content-full text-center">
                        <div>
                            <div className="mb-15">
                                <a className="img-link" onClick={() => upload('profile', null, null)}>
                                    <img
                                        className="img-avatar img-avatar96 img-avatar-thumb"
                                        src={profilePic || 'img/avatars/avatar15.jpg'}
                                        alt="profile-picture"
                                    />
                                </a>
                            </div>

                            <h1 className="h3 text-white font-w700 mb-10">{user.firstName} {user.lastName}</h1>
                            <h3 className="h5 text-white-op" style={{ marginBottom: 3 }}>{user.email}</h3>
                            <h4 className="h6 text-white-op" style={{ marginTop: 0 }}>{user.phoneNumber}</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row" style={{ marginTop: 30 }}>
                <div className="col-md-6">
                    <div className="block">
                        <div className="block-header block-header-default">
                            <h3 className="block-title">Basic Info</h3>
                        </div>

                        <div className="block-content">
                            <Form>
                                <Form.Item>
                                    <Input size="large" value={user.firstName} style={styles.inputTwo} placeholder="First Name" readOnly />
                                </Form.Item>

                                <Form.Item>
                                    <Input size="large" value={user.lastName} style={styles.inputTwo} placeholder="Last Name" readOnly />
                                </Form.Item>

                                <Form.Item>
                                    <Input size="large" value={user.email} style={styles.inputTwo} placeholder="Email" readOnly />
                                </Form.Item>

                                <Form.Item>
                                    <Input size="large" value={user.phoneNumber} style={styles.inputTwo} placeholder="Phone Number" readOnly />
                                </Form.Item>
                            </Form>
                        </div>
                    </div>
                </div>

                <div className="col-md-6">
                    <div className="block">
                        <div className="block-header block-header-default">
                            <h3 className="block-title">Change Password</h3>
                        </div>

                        <div className="block-content">
                            <Form
                                onFinish={changePassword}
                            >
                                <Form.Item
                                    name="oldPassword"
                                    rules={[{ required: true, message: 'Enter Current Password' }]}
                                >
                                    <Input.Password placeholder="Current Password" style={styles.inputTwo} />
                                </Form.Item>

                                <Form.Item
                                    name="newPassword"
                                    rules={[{ required: true, message: 'Enter New Password' }]}
                                >
                                    <Input.Password placeholder="New Password" style={styles.inputTwo} />
                                </Form.Item>

                                <Form.Item
                                    name="confirmPassword"
                                    rules={[{ required: true, message: 'Confirm New Password' }]}
                                >
                                    <Input.Password placeholder="Confirm New Password" style={styles.inputTwo} />
                                </Form.Item>

                                <Form.Item>
                                    <Button
                                        style={{ ...styles.btnLg, width: '50%', marginBottom: 20 }}
                                        shape="round"
                                        size="large"
                                        type="primary"
                                        htmlType="submit"
                                        loading={changing}
                                    >
                                        Submit
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>

            {
                loading ? <h4 style={{ textAlign: 'center' }}>LOADING KYC DOCUMENTS....</h4>
                    : docArray.map((item) => {
                        return (
                            <div key={item.id}>
                                <h2 className="content-heading">
                                    {item.currency}
                                </h2>
                                <div className="row items-push">
                                    {item.currency === 'NGN' ? <div className="col-md-6 col-xl-3 cursor" onClick={() => onBvnClick(item.currency, item.bvnKycStatus)}>
                                        <div className="block block-rounded text-center">
                                            <div className="block-content block-content-full">
                                                {statusIcon(item.bvnKycStatus)}
                                            </div>
                                            <div className="block-content block-content-full block-content-sm bg-body-light">
                                                <div className="font-w600 mb-5">BVN</div>
                                                <div className="font-size-sm text-muted">{item.bvnKycStatus}</div>
                                            </div>
                                        </div>
                                    </div> : null}
                                    <div className="col-md-6 col-xl-3 cursor" onClick={() => upload('passport', item, item.passportPhotoKycStatus)}>
                                        <div className="block block-rounded text-center">
                                            <div className="block-content block-content-full">
                                                {statusIcon(item.passportPhotoKycStatus)}
                                            </div>
                                            <div className="block-content block-content-full block-content-sm bg-body-light">
                                                <div className="font-w600 mb-5">PASSPORT PHOTO</div>
                                                <div className="font-size-sm text-muted">{item.passportPhotoKycStatus}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-xl-3 cursor" onClick={() => upload('bill', item, item.utilityBillKycStatus)}>
                                        <div className="block block-rounded text-center">
                                            <div className="block-content block-content-full">
                                                {statusIcon(item.utilityBillKycStatus)}
                                            </div>
                                            <div className="block-content block-content-full block-content-sm bg-body-light">
                                                <div className="font-w600 mb-5">UTILITY BILL</div>
                                                <div className="font-size-sm text-muted">{item.utilityBillKycStatus}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-xl-3 cursor" onClick={() => upload('validId', item, item.validIdKycStatus)}>
                                        <div className="block block-rounded text-center">
                                            <div className="block-content block-content-full">
                                                {statusIcon(item.validIdKycStatus)}
                                            </div>
                                            <div className="block-content block-content-full block-content-sm bg-body-light">
                                                <div className="font-w600 mb-5">VALID ID</div>
                                                <div className="font-size-sm text-muted">{item.validIdKycStatus}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        )
                    })
            }

            <input type="file" id="imageFile" accept="image/*" style={{ display: 'none' }} />

            <Modal
                visible={bvnModal}
                footer={null}
                title="Enter Your BVN"
                onCancel={() => setBvnModal(false)}
            >
                <Form
                    onFinish={submitBvn}
                >
                    <Form.Item
                        name="bvnOrIdentificationNumber"
                        rules={[{ required: true, message: 'Enter BVN' }]}
                    >
                        <Input placeholder="BVN" type="tel" size="large" style={styles.inputTwo} />
                    </Form.Item>

                    <Form.Item>
                        <Button
                            style={{ ...styles.btnLg, width: '50%', marginBottom: 20 }}
                            shape="round"
                            size="large"
                            type="primary"
                            htmlType="submit"
                            loading={uploadingBvn}
                        >
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>

            <Modal
                visible={uploading}
                footer={null}
                onCancel={null}
                closable={false}
                centered
            >
                <div className="uploading">
                    <Spin size="large" />
                    Uploading...
                </div>
            </Modal>

            <style jsx>
                {`
                    .uploading {
                        display: flex;
                        flex-direction: column;
                        align-items: center;
                        justify-content: center;
                        height: 150px;
                    }

                    .mb-5 {
                        font-size: .9rem;
                    }
                    .text-muted {
                        font-size: .7rem !important;
                    }
                    .cursor {
                        cursor: pointer;
                    }
                `}
            </style>
        </DashboardLayout>
    )
}

export default withRouter(Profile)