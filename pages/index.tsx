import { Button, Input, Form, Select } from 'antd';
import '../styles/home.less';
import Link from 'next/link';
import sharedService from '../services/shared-service';
import { Features } from '../components/home/Features';
import { Solution } from '../components/home/Solution';
import { OnMobile } from '../components/home/OnMobile';
import { Footer } from '../components/home/Footer';
import { Testimonials } from '../components/home/Testimonials';
import { MenuOutlined } from '@ant-design/icons';
import { Subscribe } from '../components/home/Subscribe';
import { HomeFund } from '../components/home/HomeFund';
import { withoutAuth } from '../hocs/withoutAuth';
import { useEffect, useState } from 'react';
import { StorageService } from '../services/storage-service';
import { RequestService } from '../services/request-service';
import { MarketTable } from '../components/MarketTable';
import { ContractSearch } from '../components/ContractSearch';
import Head from 'next/head';

const Index = () => {
    const [loading, setLoading] = useState(true);
    const [bids, setBids] = useState([]);

    useEffect(() => {
        StorageService.removeSavedFund();
        localStorage.removeItem('@goto');
        loadBids();
    }, []);

    const onScrollLink = (elementId: string) => {
        document.getElementById(elementId).scrollIntoView({ behavior: 'smooth' });
    };

    const loadBids = async () => {
        setLoading(true);
        const res = await RequestService.openBids();
        setLoading(false);
        setBids(res);
    }

    return (
        <>
            <Head>
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossOrigin="anonymous" />
            </Head>
            <main style={{ background: '#fff' }}>
                <nav className="navbar navbar-expand-lg navbar-light bg-light linear-gradient nav-header">
                    <a className="navbar-brand" href="/">
                        <img src="images/logo.png" alt="logo" />
                    </a>

                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <MenuOutlined style={{ color: '#fff' }} />
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto" style={{ margin: '0 auto' }}>
                            <li className="nav-item">
                                <a className="nav-link" onClick={() => onScrollLink('about')}>About</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" onClick={() => onScrollLink('solution')}>Our Solution</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" onClick={() => onScrollLink('download')}>Download</a>
                            </li>
                        </ul>

                        <div>
                            <Link href="/login"><a style={{ color: '#fff' }}>Login</a></Link>
                            <br className="separator" />
                            <Link href="/register">
                                <Button className="get-started" size="large" type="primary">Get Started</Button>
                            </Link>
                        </div>
                    </div>
                </nav>

                <div>
                    <section className="section-one linear-gradient">
                        <div className="row">
                            <div className="col-md-2"></div>

                            <div className="col-md-4 move-right">
                                <div className="title">
                                    <span>Peer to Peer</span>
                                    <br />
                                    <span style={{ color: '#fff' }}>Currency Market
                                <br />Place for Africa
                                </span>
                                </div>

                                <p className="home-subtitle">
                                    Paaro's mission is to meet the currency needs of Africans all
                            <br />over the world through a reliable marketplace built on the <br />blockchain
                        </p>
                            </div>

                            <div className="col-md-6">
                                <div className="form-box linear-gradient">
                                    <p>Fund Transfer</p>
                                    <HomeFund />
                                </div>
                            </div>
                        </div>
                    </section>

                    <Features />

                    <Solution />

                    {
                        bids.length > 0 ?
                            <div className="marketplace-table-div">
                                <p style={{ textAlign: 'center', fontSize: '1.5rem' }}>Marketplace</p>
                                <MarketTable bids={bids} />
                            </div> : null
                    }

                    <ContractSearch />

                    <OnMobile />

                    <Testimonials />

                    <Subscribe />
                </div>

                <Footer />
            </main>
        </>
    )
}

export default withoutAuth(Index)
