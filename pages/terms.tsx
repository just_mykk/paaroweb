import DashboardLayout from "../layouts/DashboardLayout"

export default () => (
    <DashboardLayout>
        <h1 className="content-heading">Terms and Conditions</h1>

        <h3>1. What you cannot do</h3>
        <h5>
            1.1 You may use our Services only for lawful purposes. You may not use our Services:
                </h5>
        <ul>
            <li>in any way that breaches any applicable local, national or international law or regulation or causes My Paaro Limited to breach any applicable law or regulation;</li>
            <li>in any way that is unlawful or fraudulent, or has any unlawful or fraudulent purpose or effect;</li>
            <li>for the purpose of harming or attempting to harm minors in any way;</li>
            <li>for anything that is abusive or does not comply with our content standards;</li>
            <li>for any unsolicited or unauthorised advertising or promotional material or any other form of spam; or</li>
            <li>to deal in harmful programs like viruses or spyware or similar computer code designed to adversely affect the operation of any computer software or hardware;</li>
            <li>in any way that would locally or internationally evade any applicable taxes or facilitate tax evasion.</li>
        </ul>
        <h5>1.2 We do not provide our Services to businesses or support transactions which involve:</h5>
        <ul>
            <li>tobacco, narcotics, steroids, cannabis, certain controlled substances or other products that present a risk to consumer safety;</li>
            <li>drug paraphernalia (equipment, product, or material that is modified for making, using, or concealing drugs) or transactions which involve or related to pharmaceuticals;</li>
            <li>seeds or plants;</li>
            <li>chemicals;</li>
            <li>military & semi-military goods & services (including weapons, military software or technologies);</li>
            <li>adult content;</li>
            <li>bitcoin or other cryptocurrencies;</li>
            <li>binary options; or</li>
            <li>individuals, entities or countries which are subject to international sanctions;</li>
            <li>smart drugs / nootropics;</li>
            <li>plagiarism services, multi-level marketing schemes, CFD/options traders located in offshore;</li>
            <li>winnings/gambling payments;</li>
            <li>lotteries, syndicates;</li>
            <li>illegal activities, support of terrorism (including eco-terrorism and groups that support similar such activities), extremism, violence, insurgency;</li>
        </ul>
        <h5>1.3 We do not provide our Services to the following categories of entities:</h5>
        <ul>
            <li>Money Service Businesses cashing cheques which are made payable to customers.</li>
            <li>Shell banks: banks that do not have a physical presence in any country.</li>
        </ul>
        <h5>1.4 You also agree:</h5>
        <ul>
            <li>not to copy or use any part of our Services; and</li>
            <li>
                not to access without authority, interfere with, damage or disrupt:
                        <ol>
                    <li>any part of our Services;</li>
                    <li>any equipment or network on which our Website is stored;</li>
                    <li>any software used in the provision of our Services; or</li>
                    <li>any equipment or network or software owned or used by any third party.</li>
                </ol>
            </li>
        </ul>
        <h5>1.5 You may only use your My Paaro Limited Account Number (as we provided to you) to receive funds into your My Paaro Limited  Wallet for the following purposes:</h5>
        <ul>
            <li>Sending or Receiving payments from family, friends or other people you know for personal purposes; or</li>
        </ul>
        <h5>1.6 You shall not use your My Paaro Limited Account for the following purpose:</h5>
        <ul>
            <li>conducting your business or using the Services in a manner that is likely to result in complaints, disputes, reversals, chargebacks or other liability to My Paaro Limited, other Customers, third parties or you.</li>
        </ul>
        <br />

        <h3>2. Suspension and termination</h3>
        <h5>2.1 We alone will determine whether there has been a breach of this acceptable use policy through your use of our Services.</h5><br />
        <h5>2.2 We take breach of this policy seriously and may take the following actions:</h5>
        <ul>
            <li>immediate, temporary or permanent withdrawal of your right to use our Services;</li>
            <li>suspend or cancel your payment orders and take such other actions as we consider necessary;</li>
            <li>immediate, temporary or permanent removal of any posting or material uploaded by you;</li>
            <li>issue of a warning;</li>
            <li>legal action against you including proceedings for reimbursement of all costs on an “all expenses” basis; and/or</li>
            <li>reporting and disclosure of information to law enforcement authorities.</li>
        </ul>
        <br />

        <h3>3. Changes to the acceptable use policy</h3>
        <h5>3.1 We may revise this acceptable use policy at any time by amending this page. You will want to check it regularly as it is legally binding on you.</h5>

        <style jsx>
            {`
                h3, h5 {
                    font-family: Montserrat;
                }

                h3 {
                    font-weight: bold;
                }
            `}
        </style>
    </DashboardLayout>
)