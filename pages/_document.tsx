import Document, { Head, Html, Main, NextScript } from 'next/document';
import React from "react";

export default class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx);
        return { ...initialProps };
    }

    render() {
        return (
            <Html>
                <Head>
                    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,400,500,700&display=swap"
                        rel="stylesheet" />
                    <link rel="stylesheet" href="js/plugins/datatables/dataTables.bootstrap4.min.css" />
                    <link rel="stylesheet" id="css-main" href="css/codebase.min.css" />

                    <link rel="shortcut icon" href="favicon/favicon.ico" />
                    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png" />
                    <link rel="icon" type="image/png" sizes="32x132" href="favicon/favicon-32x32.png" />
                    <link rel="apple-touch-icon" href="favicon/apple-touch-icon.png" />
                </Head>

                <body>
                    <Main />
                    <NextScript />

                    <script src="js/core/jquery.min.js"></script>
                    <script src="js/core/bootstrap.bundle.min.js"></script>
                    <script src="js/core/jquery.slimscroll.min.js"></script>
                    <script src="js/core/jquery.scrollLock.min.js"></script>
                    <script src="js/core/jquery.appear.min.js"></script>
                    <script src="js/core/jquery.countTo.min.js"></script>
                    <script src="js/core/js.cookie.min.js"></script>
                    <script src="js/codebase.js"></script>
                    <script src="js/plugins/chartjs/Chart.bundle.min.js"></script>
                    <script src="js/pages/be_pages_ecom_dashboard.js"></script>
                    <script src="https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-inline.js"></script>
                    <script src="js/plugins/datatables/jquery.dataTables.min.js"></script>
                    <script src="js/plugins/datatables/dataTables.bootstrap4.min.js"></script>
                </body>
            </Html>
        )
    }
}
