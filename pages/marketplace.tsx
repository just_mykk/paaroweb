import DashboardLayout from "../layouts/DashboardLayout"
import { withAuth } from "../hocs/withAuth"
import { useEffect, useState } from "react"
import { AppLoader } from "../components/AppLoader";
import { RequestService } from "../services/request-service";
import { MarketTable } from "../components/MarketTable";
import { ContractSearch } from "../components/ContractSearch";

const Marketplace = () => {
    const [loading, setLoading] = useState(true);
    const [bids, setBids] = useState([]);

    useEffect(() => {
        localStorage.removeItem('@goto');
        loadData();
    }, []);

    const loadData = async () => {
        setLoading(true);
        const res = await RequestService.openBids();
        setLoading(false);
        setBids(res);
    }

    // if (loading) return <AppLoader />;

    return (
        <>
            <div style={{ display: loading ? 'block' : 'none' }}><AppLoader /></div>
            
            <div style={{ display: loading ? 'none' : 'block' }}>
                <DashboardLayout>
                    <h2 className="content-heading">Open Bids</h2>

                    <ContractSearch />

                    <div className="block">
                        <div className="block-header block-header-default">
                            <h3 className="block-title">Bids</h3>
                        </div>

                        <div className="block-content block-content-full">
                            <MarketTable bids={bids} />
                        </div>
                    </div>

                </DashboardLayout>
            </div>
        </>
    )
}

export default withAuth(Marketplace)
