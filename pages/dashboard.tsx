/** @format */

import { NextPage } from 'next';
import DashboardLayout from '../layouts/DashboardLayout';
import { TransactionService, BeneficiaryModel } from '../services/tansaction-service';
import { useEffect, useState } from 'react';
import { GlobalService } from '../services/global-service';
import { AppLoader } from '../components/AppLoader';
import { withAuth } from '../hocs/withAuth';
import sharedService from '../services/shared-service';
import { Button, Modal, Divider, Spin } from 'antd';
import moment from 'moment';
import OnboardingService from '../services/onboarding-service';
import Swal from 'sweetalert2';
import Router from 'next/router';

const Dashboard: NextPage = () => {
  const [pending, setPending] = useState([]);
  const [cancelled, setCancelled] = useState([]);
  const [completed, setCompleted] = useState([]);
  const [tranInView, setTranInView] = useState([]);
  const [selectedTran, setSelectedTran] = useState('Pending');
  const [loading, setLoading] = useState(true);
  const [showModal, setShowModal] = useState(false);
  const [loadingDetails, setLoadingDetails] = useState(false);
  const [tranItem, setTranItem] = useState<any>(undefined);
  const [benItem, setBenItem] = useState<BeneficiaryModel>(new BeneficiaryModel());

  const tranService: TransactionService = new TransactionService();

  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => {
    setLoading(true);
    const pending = await tranService.pending();
    const cancelled = await tranService.cancelled();
    const completed = await tranService.completed();
    await getUserInfo();
    setLoading(false);

    setPending(pending);
    setTranInView(pending);
    setCancelled(cancelled);
    setCompleted(completed);
  };

  const getUserInfo = async () => {
    const user = await OnboardingService.getUser();
    if (user) {
      if (!user.kycComplete) {
        Swal.fire({
          title: 'KYC',
          icon: 'info',
          text: 'Kindly complete your KYC details',
        }).then((val) => {
          if (val.isConfirmed) Router.push('/profile');
        });
      }
    }
  };

  const onSummarySelect = (tranType: string, tranList: Array<any>) => {
    setSelectedTran(tranType);
    setTranInView(tranList);
  };

  const viewDetails = async (item: any) => {
    setLoadingDetails(true);
    const res = await tranService.details(item.id, item.transferType);
    setLoadingDetails(false);

    setTranItem(item);
    setShowModal(true);
    setBenItem(res);
  };

  if (loading) return <AppLoader />;

  return (
    <Spin spinning={loadingDetails} size="large">
      <DashboardLayout title="Dashboard">
        <div className="content-heading">Transaction Summary</div>

        <div className="row gutters-tiny">
          {/* Pending */}
          <div className="col-md-6 col-xl-4" onClick={() => onSummarySelect('Pending', pending)}>
            <a className="block block-rounded block-transparent linear-gradient">
              <div className="block-content block-content-full block-sticky-options">
                <div className="py-20 text-center">
                  <div className="font-size-h2 font-w700 mb-0 text-white">{pending.length}</div>
                  <div className="font-size-sm font-w600 text-uppercase text-white-op">Pending</div>
                </div>
              </div>
            </a>
          </div>
          {/* END Pending */}
          {/* Canceled */}
          <div
            className="col-md-6 col-xl-4"
            onClick={() => onSummarySelect('Cancelled', cancelled)}
          >
            <a className="block block-rounded block-transparent gbp-gradient">
              <div className="block-content block-content-full block-sticky-options">
                <div className="py-20 text-center">
                  <div className="font-size-h2 font-w700 mb-0 text-white">{cancelled.length}</div>
                  <div className="font-size-sm font-w600 text-uppercase text-white-op">
                    Cancelled
                  </div>
                </div>
              </div>
            </a>
          </div>
          {/* END Canceled */}
          {/* Completed */}
          <div
            className="col-md-6 col-xl-4"
            onClick={() => onSummarySelect('Completed', completed)}
          >
            <a className="block block-rounded block-transparent usd-gradient">
              <div className="block-content block-content-full block-sticky-options">
                <div className="py-20 text-center">
                  <div className="font-size-h2 font-w700 mb-0 text-white">{completed.length}</div>
                  <div className="font-size-sm font-w600 text-uppercase text-white-op">
                    Completed
                  </div>
                </div>
              </div>
            </a>
          </div>
          {/* END Completed */}
        </div>

        <div className="content-heading">
          {selectedTran} Transactions({tranInView.length})
        </div>
        <div className="block block-rounded">
          <div className="block-content">
            <table className="table table-borderless table-striped">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Tran Type</th>
                  <th className="d-none d-sm-table-cell">Fee</th>
                  <th className="text-right">Amount</th>
                  <th className="text-center" />
                </tr>
              </thead>

              <tbody>
                {tranInView.map((item) => {
                  return (
                    <tr key={item.id}>
                      <td>{moment(item.dateCreated).format('lll')}</td>
                      <td>{item.transferType}</td>
                      <td>
                        {sharedService.currSymbol(item.feeCurrency)}
                        {GlobalService.addComma(item.feeAmount)}
                      </td>
                      <td className="text-right">
                        <span className="font-w600" style={{ color: 'var(--primary-color)' }}>
                          {sharedService.currSymbol(item.wallet.currency.type)}
                          {GlobalService.addComma(item.valueAmount)}
                        </span>
                      </td>

                      <td className="text-center">
                        <Button
                          onClick={() => viewDetails(item)}
                          type="link"
                          style={{ fontSize: '.8rem' }}
                        >
                          View Details
                        </Button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>

        <Modal
          visible={showModal}
          onCancel={() => setShowModal(false)}
          footer={null}
          centered
          title={<h5 className="modal-title">Transaction Details</h5>}
        >
          {tranItem ? (
            <>
              <div style={{ padding: '0 50px' }}>
                <div className="table-modal-list">
                  <span>Date</span>
                  <span>{moment(tranItem.dateCreated).format('lll')}</span>
                </div>
                <Divider />

                <div className="table-modal-list">
                  <span>Tran Type</span>
                  <span>{tranItem.transferType}</span>
                </div>
                <Divider />

                <div className="table-modal-list">
                  <span>Amount</span>
                  <span>
                    {sharedService.currSymbol(tranItem.wallet.currency.type)} {tranItem.valueAmount}
                  </span>
                </div>
                <Divider />

                <div className="table-modal-list">
                  <span>Fee</span>
                  <span>
                    {sharedService.currSymbol(tranItem.feeCurrency)}
                    {GlobalService.addComma(tranItem.feeAmount)}
                  </span>
                </div>
                <Divider />

                <div className="table-modal-list">
                  <span>Account Name</span>
                  <span>{benItem.beneficiaryName || 'N/A'}</span>
                </div>
                <Divider />

                <div className="table-modal-list">
                  <span>Account Number</span>
                  <span>{benItem.beneficiaryAccount || 'N/A'}</span>
                </div>
                <Divider />

                <div className="table-modal-list">
                  <span>Bank Name</span>
                  <span>{benItem.bankName || 'N/A'}</span>
                </div>
              </div>
            </>
          ) : null}
        </Modal>

        <style jsx>
          {`
            th {
              font-weight: 400 !important;
              font-size: 0.9rem;
              color: var(--primary-color);
            }

            tr {
              font-weight: 400;
              font-size: 0.8rem;
            }

            .modal-title {
              font-family: Montserrat;
              color: var(--primary-color);
              text-align: center;
            }
          `}
        </style>
      </DashboardLayout>
    </Spin>
  );
};

export default withAuth(Dashboard);
