import DashboardLayout from '../layouts/DashboardLayout';
import {
    Button,
    Form,
    InputNumber,
    Input,
    Select,
    Modal,
    Result,
    Alert,
} from 'antd';
import { withAuth } from '../hocs/withAuth';
import { useState, useEffect } from 'react';
import { RequestService } from '../services/request-service';
import sharedService from '../services/shared-service';
import { GlobalService } from '../services/global-service';
import { AppLoader } from '../components/AppLoader';
import { RecipientService } from '../services/recipient-service';
import { RequestModel } from '../models/request-model';
import { StorageService } from '../services/storage-service';
import { MarketTable } from '../components/MarketTable';

const Request = () => {
    const [loading, setLoading] = useState(false);
    const [success, setSuccess] = useState(false);
    const [message, setMessage] = useState('');
    const [loadingPage, setLoadingPage] = useState(false);
    const [recAmount, setRecAmount] = useState(null);
    const [recCurrency, setRecCurrency] = useState('');
    const [currencies, setCurrencies] = useState([]);
    const [serverRate, setServerRate] = useState(null);
    const [fee, setFee] = useState(null);
    const [benList, setBenList] = useState([]);
    const [bids, setBids] = useState([]);
    const [error, setError] = useState('');
    const [request, setRequest] = useState<RequestModel>(new RequestModel());

    useEffect(() => {
        loadData();
    }, []);

    useEffect(() => {
        onValueAmtBlur();
    }, [request.valueCurrency]);

    useEffect(() => {
        receiverAmount();
    }, [fee, serverRate]);

    useEffect(() => {
        getBenList();
    }, [recCurrency]);

    useEffect(() => {
        const _rate = RequestService.getRate(
            currencies,
            request.valueCurrency,
            recCurrency
        );
        setServerRate(_rate);
        setRequest({ ...request, rate: _rate });
    }, [request.valueCurrency, recCurrency]);

    const loadData = async () => {
        setLoadingPage(true);
        const res = await GlobalService.getCurrencies();
        storageFund();
        setLoadingPage(false);

        setCurrencies(res);
    };

    const storageFund = () => {
        const fund = StorageService.getSavedFund();
        if (fund) {
            setRequest(fund.request);
            setRecCurrency(fund.recCurrency);
            setRecAmount(fund.recAmount);
            setFee(fund.fee);
        }
        StorageService.removeSavedFund();
    };

    const submit = async () => {
        if (!validFields()) return;

        setLoading(true);
        const body = {
            recAmount,
            recCurrency,
            ...request,
        };
        sharedService.logger('request body', body);
        const res = await RequestService.searchBids(body);
        setBids(res);
        if (res.length < 1) noOpenBid();
        setLoading(false);
    };

    const validFields = (): boolean => {
        setError('');
        if (
            request.valueAmount <= 0 ||
            !request.valueCurrency ||
            !request.rate ||
            !recCurrency ||
            !request.beneficiaryAccount
        ) {
            setError('All fields are required');
            return false;
        }

        if (request.valueCurrency === recCurrency) {
            setError('Request of the same currency is not allowed');
            return false;
        }
        return true;
    };

    const onValueCurrencyChange = val => {
        if (val === recCurrency) {
            setRecCurrency(request.valueCurrency);
        }

        if (val !== 'NGN' && recCurrency !== 'NGN') setRecCurrency('NGN');
        setRequest({ ...request, valueCurrency: val });
        resetValues();
    };

    const onRecCurrencyChange = val => {
        if (val !== 'NGN' && request.valueCurrency !== 'NGN')
            setRequest({ ...request, valueCurrency: 'NGN' });

        if (val === request.valueCurrency) {
            setRequest({ ...request, valueCurrency: val });
        }

        setRecCurrency(val);
        resetValues();
    };

    const resetValues = () => {
        setRecAmount(null);
    };

    const onValueAmtBlur = async () => {
        setFee(null);
        if (request.valueAmount > 0 && request.valueCurrency) {
            setLoading(true);
            const body = {
                currency: request.valueCurrency,
                transferType: 'TRANSFER',
                tranVolume: request.valueAmount,
            };
            const _fee = await RequestService.getFee(body);
            setLoading(false);
            setFee(_fee);
        }
    };

    const receiverAmount = () => {
        if (
            request.valueAmount > 0 &&
            request.valueCurrency &&
            fee !== null &&
            request.rate > 0
        ) {
            const val =
                request.valueCurrency === 'NGN'
                    ? Number(
                          (request.valueAmount - fee) / request.rate
                      ).toFixed(2)
                    : (request.valueAmount - fee) * request.rate;
            setRecAmount(val);
        }
    };

    const getBenList = async () => {
        if (recCurrency) {
            const res = await RecipientService.recipientByCurrency(recCurrency);
            setBenList(res);
        }
    };

    const closeModal = () => {
        setSuccess(false);
        setMessage('');
        loadData();
    };

    const newRequest = async () => {
        setLoading(true);
        const res = await RequestService.newRequest(request);
        setLoading(false);

        setMessage(res.message);
        if (res.success) setSuccess(true);
        if (!res.success) handleError(res.message);
    };

    const noOpenBid = () => {
        Modal.confirm({
            title: 'No Bid',
            content: 'No bid found. Create a new request?',
            okText: 'Yes',
            cancelText: 'No',
            onOk: () => newRequest(),
        });
    };

    const onRecipientSelect = (ev, opt) => {
        const item = benList.filter(item => item.id === Number(opt.key))[0];
        setRequest({
            ...request,
            bankId: item.bankId,
            sortCode: item.bankSortCode,
            swiftCode: item.bankSortCode,
            beneficiaryAccount: item.accountNumber,
            beneficiaryCurrency: item.accountCurrency,
            beneficiaryName: item.accountName,
        });
    };

    const handleError = (msg: string) => {
        Modal.error({
            title: 'Request Error',
            content: msg,
            okText: msg === 'All KYC has not been verified' ? 'Goto KYC' : null,
            onOk:
                msg === 'All KYC has not been verified'
                    ? () => window.open('/profile', '_self')
                    : null,
            maskClosable: true,
        });
    };

    if (loadingPage) return <AppLoader />;

    return (
        <DashboardLayout title="New Request">
            <h2 className="content-heading">Fund Request</h2>

            <div className="block">
                <div className="block-content">
                    {error ? (
                        <Alert
                            message="Request Error"
                            description={error}
                            type="error"
                            closable
                            onClose={() => setError('')}
                            style={{marginBottom: 20}}
                        />
                    ) : null}

                    <Form onFinish={submit} layout="vertical">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group row">
                                    <div className="col-md-3">
                                        <Form.Item label="Send Amount">
                                            <Input.Group size="large" compact>
                                                <InputNumber
                                                    size="large"
                                                    style={{
                                                        width: '65%',
                                                        fontSize: '.9rem',
                                                    }}
                                                    placeholder="Send Amount"
                                                    min={1}
                                                    formatter={value =>
                                                        `${value}`.replace(
                                                            /\B(?=(\d{3})+(?!\d))/g,
                                                            ','
                                                        )
                                                    }
                                                    parser={value =>
                                                        value.replace(
                                                            /\$\s?|(,*)/g,
                                                            ''
                                                        )
                                                    }
                                                    onChange={val =>
                                                        setRequest({
                                                            ...request,
                                                            valueAmount: val,
                                                        })
                                                    }
                                                    value={request.valueAmount}
                                                    onBlur={() =>
                                                        onValueAmtBlur()
                                                    }
                                                />
                                                <Select
                                                    onChange={
                                                        onValueCurrencyChange
                                                    }
                                                    size="large"
                                                    style={{
                                                        width: '35%',
                                                        fontSize: '.9rem',
                                                    }}
                                                    value={
                                                        request.valueCurrency
                                                    }
                                                >
                                                    {currencies.map(
                                                        (item, index) => {
                                                            return (
                                                                <Select.Option
                                                                    key={
                                                                        item.type +
                                                                        'V'
                                                                    }
                                                                    value={
                                                                        item.type
                                                                    }
                                                                >
                                                                    {item.type}
                                                                </Select.Option>
                                                            );
                                                        }
                                                    )}
                                                </Select>
                                            </Input.Group>
                                        </Form.Item>
                                    </div>

                                    <div className="col-md-3">
                                        <Form.Item
                                            help={`Suggested Rate - ${serverRate}`}
                                            label="Rate"
                                        >
                                            <InputNumber
                                                placeholder="Rate"
                                                style={{
                                                    width: '100%',
                                                    fontSize: '.9rem',
                                                }}
                                                size="large"
                                                formatter={value =>
                                                    `${value}`.replace(
                                                        /\B(?=(\d{3})+(?!\d))/g,
                                                        ','
                                                    )
                                                }
                                                parser={value =>
                                                    value.replace(
                                                        /\$\s?|(,*)/g,
                                                        ''
                                                    )
                                                }
                                                value={request.rate}
                                                onChange={val =>
                                                    setRequest({
                                                        ...request,
                                                        rate: val,
                                                    })
                                                }
                                                onBlur={receiverAmount}
                                            />
                                        </Form.Item>
                                    </div>

                                    <div className="col-md-3">
                                        <Form.Item label="Processing Fee">
                                            <InputNumber
                                                placeholder="Processing Fee"
                                                style={{
                                                    width: '100%',
                                                    fontSize: '.9rem',
                                                }}
                                                size="large"
                                                formatter={value =>
                                                    `${value}`.replace(
                                                        /\B(?=(\d{3})+(?!\d))/g,
                                                        ','
                                                    )
                                                }
                                                parser={value =>
                                                    value.replace(
                                                        /\$\s?|(,*)/g,
                                                        ''
                                                    )
                                                }
                                                readOnly
                                                value={fee}
                                                onChange={val => setFee(val)}
                                            />
                                        </Form.Item>
                                    </div>

                                    <div className="col-md-3">
                                        <Form.Item
                                            label="Recipient Recieves"
                                            help="Less Processing Fee"
                                        >
                                            <Input.Group size="large" compact>
                                                <InputNumber
                                                    size="large"
                                                    style={{
                                                        width: '65%',
                                                        fontSize: '.9rem',
                                                    }}
                                                    placeholder="Recipient Receives"
                                                    min={1}
                                                    formatter={value =>
                                                        `${value}`.replace(
                                                            /\B(?=(\d{3})+(?!\d))/g,
                                                            ','
                                                        )
                                                    }
                                                    parser={value =>
                                                        value.replace(
                                                            /\$\s?|(,*)/g,
                                                            ''
                                                        )
                                                    }
                                                    onChange={val =>
                                                        setRecAmount(val)
                                                    }
                                                    value={recAmount}
                                                    readOnly
                                                />
                                                <Select
                                                    onChange={
                                                        onRecCurrencyChange
                                                    }
                                                    size="large"
                                                    style={{
                                                        width: '35%',
                                                        fontSize: '.9rem',
                                                    }}
                                                    value={recCurrency}
                                                >
                                                    {currencies.map(
                                                        (item, index) => {
                                                            return (
                                                                <Select.Option
                                                                    key={
                                                                        item.type +
                                                                        'R'
                                                                    }
                                                                    value={
                                                                        item.type
                                                                    }
                                                                >
                                                                    {item.type}
                                                                </Select.Option>
                                                            );
                                                        }
                                                    )}
                                                </Select>
                                            </Input.Group>
                                        </Form.Item>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="col-md-6">
                                <Form.Item
                                    label="Recipient"
                                    style={{ marginBottom: 0 }}
                                >
                                    <Select
                                        size="large"
                                        showSearch
                                        style={{ fontSize: '.9rem' }}
                                        onChange={onRecipientSelect}
                                    >
                                        {benList.map(item => {
                                            return (
                                                <Select.Option
                                                    key={item.id}
                                                    value={item.accountName}
                                                >
                                                    {item.accountName} -{' '}
                                                    {item.accountNumber}
                                                </Select.Option>
                                            );
                                        })}
                                    </Select>
                                </Form.Item>
                                <a href="/recipient" className="add-link">
                                    Add New Recipient
                                </a>
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="col-12">
                                <Button
                                    style={{ width: '150px' }}
                                    shape="round"
                                    size="large"
                                    type="primary"
                                    htmlType="submit"
                                    loading={loading}
                                >
                                    Submit
                                </Button>
                            </div>
                        </div>
                    </Form>
                </div>
            </div>

            {bids.length > 0 ? (
                <>
                    <h2 className="content-heading">Available Bids</h2>
                    <div className="block">
                        <div className="block-content">
                            <MarketTable bids={bids} />
                        </div>
                    </div>
                </>
            ) : null}

            <Modal
                centered
                visible={success}
                footer={null}
                onCancel={closeModal}
            >
                <Result
                    status="success"
                    title="Success"
                    subTitle={message}
                    extra={[
                        <Button onClick={closeModal} type="primary">
                            Done
                        </Button>,
                    ]}
                />
            </Modal>
        </DashboardLayout>
    );
};

export default withAuth(Request);
