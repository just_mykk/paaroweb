/** @format */

import DashboardLayout from '../layouts/DashboardLayout';
import { useEffect, useState } from 'react';
import { RecipientService } from '../services/recipient-service';
import { AppLoader } from '../components/AppLoader';
import { Button, Form, Input, Select, notification } from 'antd';
import { PlusCircleOutlined } from '@ant-design/icons';
import styles from '../custom_styles';
import { GlobalService } from '../services/global-service';
import { StorageService } from '../services/storage-service';
import sharedService from '../services/shared-service';
import { withAuth } from '../hocs/withAuth';

const { Option } = Select;

const Recipient = () => {
  const [recipients, setRecipients] = useState([]);
  const [countries, setCountries] = useState([]);
  const [banks, setBanks] = useState([]);
  const [currencies, setCurrencies] = useState([]);
  const [loading, setLoading] = useState(true);
  const [showForm, setShowForm] = useState(false);
  const [adding, setAdding] = useState(false);
  const [error, setError] = useState('');

  const [countryCode, setCountryCode] = useState('');
  const [bankId, setBankId] = useState('');
  const [accountCurrency, setAccountCurrency] = useState('');
  const [accountName, setAccountName] = useState('');
  const [accountNumber, setAccountNumber] = useState('');
  const [bankSortCode, setBankSortCode] = useState('');
  const [currencyId, setCurrencyId] = useState(0);
  const [bankName, setBankName] = useState('');

  useEffect(() => {
    loadApp();
  }, []);

  const loadApp = async () => {
    setLoading(true);
    const _recipients = await RecipientService.get();
    setLoading(false);
    setRecipients(_recipients);

    const _countries = await GlobalService.getCountries();
    const _currencies = await GlobalService.getCurrencies();

    setCurrencies(_currencies);
    setCountries(_countries);
  };

  const onCountryChange = async (val) => {
    setCountryCode(val);
    const _banks = await GlobalService.getBanksByCountry(val);
    setBanks(_banks);
  };

  const onCurrencyChange = (val, opt) => {
    setAccountCurrency(opt.children);
    setCurrencyId(opt.value);
  };

  const onBankChange = (val, opt) => {
    setBankId(opt.value);
    setBankName(opt.children);
  };

  const submitForm = async () => {
    setError('');
    if (!validFields()) return;

    const customerId = StorageService.getUserId();
    const body = {
      accountCurrency,
      accountName,
      accountNumber,
      bankId,
      bankSortCode,
      currencyId,
      customerId,
      bankName,
    };

    setAdding(true);
    const res = await RecipientService.add(body);
    setAdding(false);

    if (!res) {
      setShowForm(false);
      showNotification('success', 'Recipient added successfully');
      loadApp();
    }
    if (res) setError(res);
  };

  const onAccountBlur = async () => {
    setError('');
    if (bankId && accountNumber.length >= 10 && accountCurrency === 'NGN') {
      const body = { bankId, accountNumber };
      setAdding(true);
      const res = await RecipientService.validateAccount(body);
      setAdding(false);

      if (res.respCode === 99) setError(res.respDesc);
      if (res.respCode === 0) setAccountName(res.body.accountName);
    }
  };

  const validFields = (): boolean => {
    if (!countryCode) {
      setError('Select Country');
      return false;
    }

    if (!bankName) {
      setError('Select Bank');
      return false;
    }

    if (currencyId <= 0) {
      setError('Select Currency');
      return false;
    }
    if (!accountNumber) {
      setError('Enter Account Number');
      return false;
    }
    if (accountCurrency !== 'NGN' && !accountName) {
      setError('Enter Account Name');
      return false;
    }
    if (accountCurrency !== 'NGN' && !bankSortCode) {
      setError('Enter Sort Code');
      return false;
    }

    return true;
  };

  const onDelete = async (id: number) => {
    const res = await RecipientService.delete(id);
    if (res.error) showNotification('error', res.message);
    if (!res.error) {
      loadApp();
      showNotification('success', res.message);
    }
  };

  const showNotification = (type: string, msg: string) => {
    notification[type]({
      description: msg,
    });
  };

  const newForm = () => {
    return (
      <>
        <div className="block">
          <div className="block-header block-header-default">
            <h2 className="block-title">New Recipient</h2>
            <div className="block-options">
              <button type="button" className="btn-block-option" onClick={() => setShowForm(false)}>
                <i className="fa fa-times" />
              </button>
            </div>
          </div>

          <div className="block-content">
            <div className="row justify-content-center py-20">
              <div className="col-xl-6">
                <div className="error-div">{error}</div>
                <Form layout="vertical" onFinish={submitForm}>
                  <Form.Item>
                    <Select
                      onChange={onCountryChange}
                      placeholder="Select Country"
                      size="large"
                      style={{ fontSize: '.9rem' }}
                    >
                      {countries.map((item) => (
                        <Option key={item.country} value={item.country}>
                          {item.country}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>

                  {countryCode ? (
                    <Form.Item>
                      <Select
                        onChange={onBankChange}
                        placeholder="Select Bank"
                        size="large"
                        style={{ fontSize: '.9rem' }}
                      >
                        {banks.map((item) => {
                          return (
                            <Option key={item.id} value={item.id}>
                              {item.bankName}
                            </Option>
                          );
                        })}
                      </Select>
                    </Form.Item>
                  ) : null}

                  <Form.Item>
                    <Select
                      onChange={onCurrencyChange}
                      style={{ fontSize: '.9rem' }}
                      size="large"
                      placeholder="Select Currency"
                    >
                      {currencies.map((item) => {
                        return (
                          <Option key={'c' + item.id} value={item.id}>
                            {item.type}
                          </Option>
                        );
                      })}
                    </Select>
                  </Form.Item>

                  <Form.Item>
                    <Input
                      value={accountNumber}
                      onChange={(val) => setAccountNumber(val.target.value)}
                      placeholder="Account Number"
                      type="tel"
                      size="large"
                      style={styles.inputTwo}
                      onBlur={onAccountBlur}
                    />
                  </Form.Item>

                  <Form.Item>
                    <Input
                      value={accountName}
                      onChange={(val) => setAccountName(val.target.value)}
                      placeholder="Account Name"
                      readOnly={accountCurrency && accountCurrency === 'NGN'}
                      size="large"
                      style={styles.inputTwo}
                    />
                  </Form.Item>

                  {accountCurrency && accountCurrency != 'NGN' ? (
                    <Form.Item>
                      <Input
                        placeholder="Sort Code"
                        type="tel"
                        size="large"
                        style={styles.inputTwo}
                        onChange={(val) => setBankSortCode(val.target.value)}
                      />
                    </Form.Item>
                  ) : null}

                  <Button
                    style={{ ...styles.btnLg, width: '50%', marginBottom: 20 }}
                    shape="round"
                    size="large"
                    type="primary"
                    htmlType="submit"
                    loading={adding}
                  >
                    Submit
                  </Button>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };

  if (loading) return <AppLoader />;

  return (
    <DashboardLayout title="Recipients">
      {showForm ? newForm() : null}

      <div className="block">
        <div className="block-header block-header-default">
          <Button type="primary" onClick={() => setShowForm(!showForm)}>
            <PlusCircleOutlined />
            Add New Recipient
          </Button>
        </div>

        <div className="block-content">
          <div className="table-responsive">
            <table className="table table-striped table-vcenter">
              <thead>
                <tr>
                  <th className="text-center" style={{ width: '100px' }}>
                    <i className="si si-user"></i>
                  </th>
                  <th>Account Name</th>
                  <th>Account No</th>
                  <th>Bank</th>
                  <th>Currency</th>
                  <th>Sort Code</th>
                  <th className="text-center" style={{ width: '100px' }}>
                    Actions
                  </th>
                </tr>
              </thead>

              <tbody>
                {recipients.map((item) => (
                  <tr key={item.id}>
                    <td className="text-center">
                      <img
                        className="img-avatar img-avatar48"
                        src="img/avatars/avatar16.jpg"
                        alt=""
                      />
                    </td>
                    <td className="font-w600">{item.accountName}</td>
                    <td>{item.accountNumber}</td>
                    <td>{item.bankName}</td>
                    <td>{item.accountCurrency}</td>
                    <td>{item.bankSortCode || 'N/A'}</td>
                    <td className="text-center">
                      <div className="btn-group">
                        <button
                          type="button"
                          className="btn btn-sm btn-secondary"
                          data-toggle="tooltip"
                          title="Delete"
                          onClick={() => onDelete(item.id)}
                        >
                          <i className="fa fa-times"></i>
                        </button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </DashboardLayout>
  );
};

export default withAuth(Recipient);
