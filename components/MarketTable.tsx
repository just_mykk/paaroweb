import { Button, Modal, Select, Result } from "antd"
import sharedService from "../services/shared-service"
import { useEffect, useState } from "react";
import { RecipientService } from "../services/recipient-service";
import { RequestService } from "../services/request-service";
import Router from 'next/router';
import moment from "moment";
import { GlobalService } from "../services/global-service";
import Head from "next/head";

interface Props {
    bids: Array<any>
}

export const MarketTable: React.FC<Props> = (props) => {
    const { bids } = props;

    const [selectedBid, setSelectedBid] = useState<any>({});
    const [showModal, setShowModal] = useState(false);
    const [loading, setLoading] = useState(false);
    const [benList, setBenList] = useState([]);
    const [request, setRequest] = useState<any>({});
    const [error, setError] = useState('');
    const [success, setSuccess] = useState(false);
    const [message, setMessage] = useState('');

    const viewBid = (item: any) => {
        if (Router.pathname === '/') {
            localStorage.setItem('@goto', '/marketplace');
            Router.push('/marketplace');
        } else {
            setShowModal(true);
            setSelectedBid(item);
            getBenList(item.wallet.currency.type);
        }
    }

    const getBenList = async (curr: string) => {
        if (curr) {
            setLoading(true);
            const res = await RecipientService.recipientByCurrency(curr);
            setLoading(false);
            setBenList(res);
        }
    }

    const onRecipientSelect = (ev, opt) => {
        const item = benList.filter((item) => item.id === Number(opt.key))[0];
        setRequest({
            matchingTransId: selectedBid.id,
            rate: selectedBid.rate,
            valueAmount: selectedBid.valueAmount,
            valueCurrency: selectedBid.wallet.currency.type,
            bankId: item.bankId,
            sortCode: item.bankSortCode,
            swiftCode: item.bankSortCode,
            beneficiaryAccount: item.accountNumber,
            beneficiaryCurrency: item.accountCurrency,
            beneficiaryName: item.accountName,
        });
    }

    const closeModal = () => {
        setShowModal(false);
        setRequest({});
    }

    const matchBid = async () => {
        setError('');

        if (!request.beneficiaryAccount) {
            setError('Select Recipient');
            return;
        }

        sharedService.logger('request body', request);
        makeRequest();
    }

    const makeRequest = async () => {
        setLoading(true);
        const res = await RequestService.newRequest(request);
        setLoading(false);

        setMessage(res.message);
        if (res.success) {
            closeModal();
            setSuccess(true);
        }
        if (!res.success) handleError(res.message);
    }

    const handleError = (msg: string) => {
        Modal.error({
            title: 'Request Error',
            content: msg,
        });
    }

    const closeSuccessModal = () => {
        location.reload();
    }

    return (
        <>
            <table className="table table-striped js-dataTable-full">
                <thead style={{ fontSize: '.8rem', color: 'var(--primary-color)' }}>
                    <tr>
                        <th className="d-none d-sm-table-cell ">USER</th>
                        <th className="text-center" style={{ width: '15%' }}>CURRENCY</th>
                        <th className="text-center">AMOUNT</th>
                        <th className="d-none d-sm-table-cell  text-center">STATUS</th>
                        <th className="d-none d-sm-table-cell ">DATE</th>
                        <th className="d-none d-sm-table-cell ">REFERENCE</th>
                        <th />
                    </tr>
                </thead>

                <tbody style={{ fontSize: '.8rem' }}>
                    {
                        bids.map((item, index) => {
                            return (
                                <tr key={index.toString()}>
                                    <td className="d-none d-sm-table-cell " style={{ color: 'var(--primary-color)' }}>
                                        <img className="table-image" src={item.customer.imageUrl} alt={item.customer.userName} />
                                        {item.customer.firstName} {item.customer.lastName}
                                    </td>
                                    <td className="font-w200 text-center">
                                        <div className="table-currency" style={{ background: sharedService.currColor(item.wallet.currency.type) }}>
                                            {sharedService.currSymbol(item.wallet.currency.type)}
                                        </div>
                                    </td>
                                    <td className="text-center">
                                        {sharedService.currSymbol(item.wallet.currency.type)} {GlobalService.addComma(item.valueAmount)}
                                        <br />
                                        <span style={{ fontSize: '.6rem' }}><span style={{ color: 'var(--primary-color)' }}>Rate:</span> {item.rate}</span>
                                    </td>
                                    <td className="d-none d-sm-table-cell  text-center">
                                        {item.status}
                                    </td>
                                    <td className="d-none d-sm-table-cell">
                                        {moment(item.dateCreated).format('lll')}
                                    </td>
                                    <td className="d-none d-sm-table-cell">
                                        {item.referenceNumber}
                                    </td>
                                    <td className="text-center">
                                        <Button onClick={() => viewBid(item)} size="small" style={{ fontSize: '.8rem' }} type="link">Buy Now</Button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>

            <Modal
                visible={showModal}
                title={<h6 className="bid-details-title">Transaction Details</h6>}
                centered
                onCancel={closeModal}
                okText="Confirm"
                onOk={matchBid}
                okButtonProps={{ loading: loading }}
                closable={!loading}
            >
                {
                    showModal ?
                        <>
                            <div style={{ padding: '0 50px' }}>
                                <div className="table-modal-list">
                                    <span>Amount</span>
                                    <span>{sharedService.currSymbol(selectedBid.wallet.currency.type)} {selectedBid.valueAmount}</span>
                                </div>

                                <div className="table-modal-list">
                                    <span>Rate</span>
                                    <span>{selectedBid.rate}</span>
                                </div>

                                <div className="table-modal-list">
                                    <span>Status</span>
                                    <span>{selectedBid.status}</span>
                                </div>

                                <div style={{ marginBottom: 20 }}>
                                    <img className="table-image" src={selectedBid.customer.imageUrl} alt={selectedBid.customer.userName} />
                                    <strong>{selectedBid.customer.firstName} {selectedBid.customer.lastName}</strong>
                                </div>

                                <div>
                                    <Select
                                        showSearch
                                        style={{ fontSize: '.9rem', width: '100%' }}
                                        onChange={onRecipientSelect}
                                        placeholder="Select Recipient"
                                    >
                                        {
                                            benList.map((item) => {
                                                return (
                                                    <Select.Option key={item.id} value={item.accountName}>
                                                        {item.accountName} - {item.accountNumber}
                                                    </Select.Option>
                                                )
                                            })
                                        }
                                    </Select>
                                    <div className="error-div">{error}</div>
                                </div>
                            </div>
                        </> : null
                }
            </Modal>

            <Modal
                centered
                visible={success}
                footer={null}
                onCancel={closeSuccessModal}
            >
                <Result
                    status='success'
                    title='Success'
                    subTitle={message}
                    extra={[<Button onClick={closeSuccessModal} type="primary">Done</Button>]}
                />
            </Modal>

            <style jsx>
                {`
                    th {
                        font-weight: 400 !important;
                    }
                `}
            </style>
        </>
    )
}
