import DashboardLayout from "../layouts/DashboardLayout";
import './loader.less';

export const AppLoader = () => {
    return (
        <DashboardLayout>
            <div className="loader-container">
                <div className="loader">Loading...</div>
            </div>

            <style>
                {`
                    .loader-container {
                        height: 90vh;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                    }
                `}
            </style>
        </DashboardLayout>
    )
}