export const OnMobile = () => {
    return (
        <section id="download">
            <div className="linear-gradient">
                <div className="download-container">
                    <div className="row">
                        {/* <div className="col-md-1">
                    </div> */}

                        <div className="col-md-6 download-app">
                            <p style={{ fontSize: '3rem', color: '#fff', margin: 0 }}>Now Available <br />on Mobile</p>
                            <div style={{ display: 'flex', alignItems: 'center' }}>
                                <a target="_blank" href='https://apps.apple.com/us/app/my-paaro/id1490433906'>
                                    <img alt='Get it on App Store' style={{ height: 40, width: 145 }} src='images/app_store.png' />
                                </a>

                                <a target="_blank" href='https://play.google.com/store/apps/details?id=com.paaro'>
                                    <img alt='Get it on Google Play' style={{ height: 60 }} src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png' />
                                </a>
                            </div>
                        </div>

                        <div className="col-md-5">
                        </div>
                    </div>
                </div>
            </div>

            <style jsx>
                {`
                    .download-container {
                        background-image: url('images/mobile_app.png');
                        background-repeat: no-repeat;
                        background-position: right;
                    }

                    @media screen and (max-width: 899px) {
                        .download-container {
                            background: none;
                        }
                    }
                `}
            </style>
        </section>
    )
}