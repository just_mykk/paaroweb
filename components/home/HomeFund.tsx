import { Form, Input, Select, InputNumber } from "antd"
import sharedService from "../../services/shared-service"
import { GlobalService } from "../../services/global-service";
import { useState, useEffect } from "react";
import { RequestModel } from "../../models/request-model";
import { RequestService } from "../../services/request-service";
import { StorageService } from "../../services/storage-service";
import Router from 'next/router';

export const HomeFund = () => {
    const [currencies, setCurrencies] = useState([]);
    const [recCurrency, setRecCurrency] = useState('');
    const [recAmount, setRecAmount] = useState(null);
    const [serverRate, setServerRate] = useState(null);
    const [fee, setFee] = useState(null);
    const [loading, setLoading] = useState(false);
    const [request, setRequest] = useState<RequestModel>(new RequestModel());

    useEffect(() => {
        loadData();
    }, []);

    useEffect(() => {
        onValueAmtBlur();
    }, [request.valueCurrency]);

    useEffect(() => {
        receiverAmount();
    }, [fee, serverRate]);

    useEffect(() => {
        const _rate = RequestService.getRate(currencies, request.valueCurrency, recCurrency);
        setServerRate(_rate);
        setRequest({ ...request, rate: _rate });
    }, [request.valueCurrency, recCurrency]);

    const loadData = async () => {
        const res = await GlobalService.getCurrencies();

        setCurrencies(res);
    }

    const onValueCurrencyChange = (val) => {
        if (val === recCurrency) {
            setRecCurrency(request.valueCurrency);
        }

        if (val !== 'NGN' && recCurrency !== 'NGN') setRecCurrency('NGN');
        setRequest({ ...request, valueCurrency: val });
        resetValues();
    }

    const onRecCurrencyChange = (val) => {
        if (val === request.valueCurrency) {
            setRequest({ ...request, valueCurrency: val });
        }

        if (val !== 'NGN' && request.valueCurrency !== 'NGN') setRequest({ ...request, valueCurrency: 'NGN' });
        setRecCurrency(val);
        resetValues();
    }

    const resetValues = () => {
        setRecAmount(null);
    }

    const onValueAmtBlur = async () => {
        setFee(null);
        if (request.valueAmount > 0 && request.valueCurrency) {
            setLoading(true);
            const body = {
                currency: request.valueCurrency,
                transferType: 'TRANSFER',
                tranVolume: request.valueAmount
            };
            const _fee = await RequestService.getFee(body);
            setLoading(false);
            setFee(_fee);
        }
    }

    const receiverAmount = () => {
        if (request.valueAmount > 0 && request.valueCurrency && fee !== null && request.rate > 0) {
            const val = request.valueCurrency === 'NGN'
                ? (request.valueAmount - fee) / request.rate
                : (request.valueAmount - fee) * request.rate;
            setRecAmount(val);
        }
    }

    const onSubmit = () => {
        const token = StorageService.getToken();
        StorageService.saveFund({request, fee, recAmount, recCurrency });
        if (token) Router.push('/dashboard');
        if (!token) Router.push('/login');
    }

    return (
        <Form
            onFinish={onSubmit}
        >
            <Form.Item>
                <Input.Group size="large" compact>
                    <InputNumber
                        size="large"
                        style={{ width: '65%', fontSize: '.9rem' }}
                        placeholder="Send Amount"
                        min={1}
                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={value => value.replace(/\$\s?|(,*)/g, '')}
                        onChange={(val) => setRequest({ ...request, valueAmount: val })}
                        value={request.valueAmount}
                        onBlur={() => onValueAmtBlur()}
                    />
                    <Select
                        onChange={onValueCurrencyChange}
                        size="large" style={{ width: '35%', fontSize: '.9rem' }}
                        value={request.valueCurrency}>
                        {
                            currencies.map((item, index) => {
                                return <Select.Option key={item.type + 'V'} value={item.type}>
                                    {item.type}
                                </Select.Option>
                            })
                        }
                    </Select>
                </Input.Group>
            </Form.Item>

            <Form.Item
                help={`Suggested Rate - ${serverRate}`}
            >
                <InputNumber
                    placeholder="Rate"
                    style={{ width: '100%', fontSize: '.9rem' }}
                    size="large"
                    formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                    parser={value => value.replace(/\$\s?|(,*)/g, '')}
                    value={request.rate}
                    onChange={(val) => setRequest({ ...request, rate: val })}
                    onBlur={receiverAmount}
                />
            </Form.Item>

            <Form.Item>
                <InputNumber
                    placeholder="Processing Fee"
                    style={{ width: '100%', fontSize: '.9rem' }}
                    size="large"
                    formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                    parser={value => value.replace(/\$\s?|(,*)/g, '')}
                    readOnly
                    value={fee}
                    onChange={(val) => setFee(val)}
                />
            </Form.Item>

            <Form.Item>
                <Input.Group size="large" compact>
                    <InputNumber
                        size="large"
                        style={{ width: '65%', fontSize: '.9rem' }}
                        placeholder="Recipient Receives"
                        min={1}
                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={value => value.replace(/\$\s?|(,*)/g, '')}
                        onChange={(val) => setRecAmount(val)}
                        value={recAmount}
                        readOnly
                    />
                    <Select
                        onChange={onRecCurrencyChange}
                        size="large" style={{ width: '35%', fontSize: '.9rem' }}
                        value={recCurrency}
                    >
                        {
                            currencies.map((item, index) => {
                                return <Select.Option key={item.type + 'R'} value={item.type}>
                                    {item.type}
                                </Select.Option>
                            })
                        }
                    </Select>
                </Input.Group>
            </Form.Item>

            <button style={_styles.btn} type="submit">Get Started</button>
        </Form>
    )
}

const _styles = {
    btn: {
        backgroundColor: '#abbc30',
        fontSize: '.8rem',
        margin: '0 auto',
        display: 'block',
        height: 35,
        width: 100,
        border: 'none',
        borderRadius: 3,
        color: '#fff',
        cursor: 'pointer'
    },
    selector: {
        width: 80
    }
}