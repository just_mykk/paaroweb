export const Subscribe = () => {
    return (
        <section>
            <div className="subscribe-container">
                <p className="sub-title">
                    Subscribe to our <br/> newsletter
                </p>

                <div className="div-form">
                    <input className="sub-input" placeholder="Email Address"/>
                    <button className="sub-btn">Subscribe</button>
                </div>
            </div>

            <style jsx>
                {`
                    .subscribe-container {
                        background-color: #0D0D0D;
                        min-height: 400px;
                        display: flex;
                        flex-direction: column;
                        justify-content: center;
                        align-items: center;
                    }

                    .sub-title {
                        color: #fff;
                        font-size: 2rem;
                        font-weight: 200;
                        text-align: center;
                    }

                    .sub-input, .sub-btn {
                        height: 45px;
                        color: #fff;
                        border: none;
                    }

                    .sub-input {
                        width: 250px;
                        background-color: #2B2B2B;
                        padding: 0 10px;
                        font-size: .8rem;
                        border-radius: 4px 0 0 4px;
                    }

                    .sub-btn {
                        width: 150px;
                        border-radius: 0 4px 4px 0;
                        background-color: #ABBE1B;
                    }

                    .div-form {
                        display: flex;
                        justify-content: center;
                        width: 100%;
                        padding: 0 20px;
                    }

                    @media screen and (max-width: 899px) {
                        .sub-input {
                            flex: 2;
                        }

                        .sub-btn {
                            flex: 1;
                        }
                    }
                `}
            </style>
        </section>
    )
}