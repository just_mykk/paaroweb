import { CSSProperties } from "react"
import { RightOutlined, LeftOutlined } from "@ant-design/icons";


export const Testimonials = () => {
    const arr = [1, 2, 3, 4];

    return (
        <section className="testimony-section">
            <div style={mainDiv}>
                <p className="testimonial-title">Testimonials</p>
                <p className="testimonial-subtitle">What People Are Saying</p>

            </div>
            <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                <div className="carousel-inner">
                    {
                        arr.map((item, index) => {
                            return (
                                <div key={`${index}C`} className={index === 0 ? 'carousel-item active' : 'carousel-item'}>
                                    <div className="testimonial-carousel-item">
                                        <p style={{ width: '50%', margin: '0 auto', textAlign: 'center' }}>
                                            {index} Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                        </p>
                                        <img src="images/testimonial.png" className="testimonial-img" />
                                        <p className="testifier">Michael Olawale</p>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
                <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <LeftOutlined />
                    <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <RightOutlined />
                    <span className="sr-only">Next</span>
                </a>
            </div>

        </section>
    )
}

const mainDiv: CSSProperties = {
    marginTop: 30,
    textAlign: 'center',
}