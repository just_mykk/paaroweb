export const Footer = () => {
    return (
        <footer>
            <div className="footer-row">
                <div className="row-item">
                    <p>&copy; {new Date().getFullYear()} Paaro Exchange. All rights reserved</p>
                </div>

                <div className="row-item">
                    <p>
                        <a href="/terms" style={{ marginRight: 0 }}>Terms and Condition</a>
                        &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                        <a href="/privacy">Privacy Policy</a>
                    </p>
                </div>
            </div>

            <style jsx>
                {`
                    .footer-row {
                        display: flex;
                        align-items: center;
                        min-height: 70px;
                        background-color: #000;
                        color: rgba(255, 255, 255, .7);
                        font-size: .8rem;
                        font-weight: 100;
                    }

                    a {
                        color: rgba(255, 255, 255, .7);
                    }

                    p {
                        margin: 0;
                    }

                    .row-item {
                        flex: 1;
                    }

                    .row-item:first-child {
                        text-align: right;
                    }

                    .row-item:last-child {
                        text-align: center;
                    }

                    @media screen and (max-width: 899px) {
                        .footer-row {
                            flex-direction: column;
                            padding-top: 20px;
                        }
                        .row-item {
                            margin-bottom: 15px;
                        }
                    }
                `}
            </style>
        </footer>
    )
}