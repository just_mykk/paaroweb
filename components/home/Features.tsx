import './features.less';

export const Features = () => {
    return (
        <section style={{ marginTop: 50 }} id="about">
            <div className="row" style={{ minHeight: '500px' }}>
                <div className="col-md-5 features" style={{paddingLeft: 0}}>
                    <img className="feature-image" src="images/features.png" />
                </div>

                <div className="col-md-1"></div>

                <div className="col-md-6 features">
                    <div className="row" style={{marginBottom: 15}}>
                        <div className="col-md-6">
                            <div className="box"></div>
                            <p className="title">Security</p>
                            <p className="label">All your financial information <br />on MyPaaro is encrypted <br />and stored securely</p>
                        </div>

                        <div className="col-md-6">
                            <div className="box"></div>
                            <p className="title">Support</p>
                            <p className="label">We are ready to help you <br />whenever you need it.</p>
                        </div>
                    </div>
                    <div className="row" >
                        <div className="col-md-6">
                            <div className="box"></div>
                            <p className="title">Speed</p>
                            <p className="label">Get our money to family and <br />friends in minutes</p>
                        </div>

                        <div className="col-md-6">
                            <div className="box"></div>
                            <p className="title">Lowest Fees</p>
                            <p className="label">Our low fees and exchange <br />rates are shown up-front</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}