export const Solution = () => {
    return (
        <section style={{ marginTop: 50 }} id="solution">
            <p style={{ textAlign: 'center', fontSize: '1.5rem' }}>Paaro's Solution</p>
            <div className="row">
                <div className="col-md-1"></div>

                <div className="col-md-6">
                    <div className="row row-margin">
                        <div className="col-md-6">
                            <p className="title">Peer to Peer Currency<br /> Merketplace</p>
                            <p className="label">
                                The Paaro Peer to Peer (P2P) platform is <br />
                                designed to match customers seeking to <br />
                                sell or buy foregin currency in selected high <br />
                                income countries to those looking to buy or <br />
                                sell foreign currency in low income<br /> countries.
                            </p>
                        </div>

                        <div className="col-md-6">
                            <p className="title">Connecting Customer</p>
                            <p className="label">
                                Essentially the Paaro platform will be <br />
                                capable of connecting customers looking to <br />
                                buy USD in Nigeria with those looking to <br />
                                buy Naira in the United States.
                            </p>
                        </div>
                    </div>

                    <div className="row row-margin">
                        <div className="col-md-6">
                            <p className="title">FX Rates</p>
                            <p className="label">
                                Additionally, FX rates will be determined <br />
                                solely by demand and supply forces with no <br />
                                hidden costs, thus ensuring a transparent <br />
                                price discovery process.
                            </p>
                        </div>

                        <div className="col-md-6">
                            <p className="title">Settlements and Fees</p>
                            <p className="label">
                                Settlements on each side of the transaction <br />
                                are made locally in each country. <br />
                                A variable fee of between 0.2% - 2% is <br />
                                charged per transaction on each of the side.
                            </p>
                        </div>
                    </div>

                    <div className="row row-margin">
                        <div className="col-md-6">
                            <p className="title">Low Transaction Cost</p>
                            <p className="label">
                                By connecting both sides of the exchange <br/>
                                through P2P, Paaro will essentially cut out <br/>
                                the spread banks and brokers typically make <br/>
                                in such transactions, thus passing these as <br/>
                                cost savings to the customers.
                            </p>
                        </div>

                        <div className="col-md-6 align-right">
                        </div>
                    </div>
                </div>

                <div className="col-md-5 align-right">
                    <img style={{ width: '100%' }} src="images/solutions.png" alt="Solutions" />
                </div>
            </div>
        </section>
    )
}