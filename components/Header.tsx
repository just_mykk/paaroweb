import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import { StorageService } from '../services/storage-service';

export const Header = () => {
    const [name, setName] = useState('');
    const [token, setToken] = useState('');

    const router = useRouter();

    useEffect(() => {
        const script = document.createElement('script');
        script.src = 'js/codebase.js';
        document.body.append(script);
        getUser();
    }, []);

    const getUser = () => {
        const _token = StorageService.getToken();
        setToken(_token);

        let user = StorageService.getUserDetails();
        if (user) {
            setName(user.firstName + ' ' + user.lastName)
        }
    }

    const logout = () => {
        document.cookie = '@paaro=;';
        router.push('/login');
    };

    return (
        <header id="page-header">
            {/* Header Content */}
            <div className="content-header">
                {/* Left Section */}
                <div className="content-header-section">
                    <button type="button" className="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="sidebar_toggle">
                        <i className="fa fa-navicon" />
                    </button>
                </div>

                {
                    token
                        ? <div className="content-header-section">
                            {/* User Dropdown */}
                            <div className="btn-group" role="group">
                                <button type="button" className="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {name}<i className="fa fa-angle-down ml-5" />
                                </button>
                                <div className="dropdown-menu dropdown-menu-right min-width-150" aria-labelledby="page-header-user-dropdown">
                                    <a className="dropdown-item" href="/profile">
                                        <i className="si si-user mr-5" /> Profile
                            </a>
                                    <div className="dropdown-divider" />

                                    <a className="dropdown-item" onClick={logout}>
                                        <i className="si si-logout mr-5" /> Sign Out
                            </a>
                                </div>
                            </div>
                        </div> : null
                }
            </div>
        </header>

    )
}