import { useState, KeyboardEvent, useEffect } from "react"
import sharedService from "../services/shared-service";
import { TransactionService } from "../services/tansaction-service";
import { Modal } from "antd";

export const ContractSearch = () => {
    const [cid, setCid] = useState('');
    const [loading, setLoading] = useState(false);
    const [details, setDetails] = useState(null);
    const [showModal, setShowModal] = useState(false);

    const onSearch = async () => {
        setLoading(true);

        const tranService = new TransactionService();
        const res = await tranService.searchBid(cid);
        setLoading(false);

        if (!res) showError();
        if (res) {
            setDetails(res);
            setShowModal(true);
        }
    }

    const showError = () => {
        Modal.error({
            title: 'Error',
            content: `Transaction with reference number ${cid} not found`
        });
    }

    const _onKeyUp = (ev: KeyboardEvent<HTMLInputElement>) => {
        if (ev.key.toLowerCase() === 'enter') onSearch();
    }

    return (
        <>
            <section>
                <div className="search-section">
                    <p style={{ textAlign: 'center', fontSize: '1.5rem' }}>Search Marketplace</p>
                    <div className="div-form">
                        <input
                            onChange={(ev) => setCid(ev.target.value)}
                            onKeyUp={_onKeyUp}
                            className="sub-input"
                            placeholder="Reference Number"
                        />
                        <button disabled={loading} onClick={onSearch} className="sub-btn">
                            {loading ? 'Searching...' : 'Search'}
                        </button>
                    </div>
                </div>
            </section>

            <Modal
                visible={showModal}
                title={<h6 className="bid-details-title">Transaction Details</h6>}
                centered
                onCancel={() => setShowModal(false)}
                okText="Print"
                onOk={() => window.print()}
            >
                {
                    showModal ?
                        <>
                            <div style={{ padding: '0 50px' }}>
                                <div className="table-modal-list">
                                    <span>Reference Number</span>
                                    <span>{details.referenceNumber}</span>
                                </div>

                                <div className="table-modal-list">
                                    <span>Amount</span>
                                    <span>{sharedService.currSymbol(details.wallet.currency.type)} {details.valueAmount}</span>
                                </div>

                                <div className="table-modal-list">
                                    <span>Rate</span>
                                    <span>{details.rate}</span>
                                </div>

                                <div className="table-modal-list">
                                    <span>Status</span>
                                    <span>{details.status}</span>
                                </div>

                                <div style={{ marginBottom: 20 }}>
                                    <img className="table-image" src={details.customer.imageUrl} alt={details.customer.userName} />
                                    <strong>{details.customer.firstName} {details.customer.lastName}</strong>
                                </div>
                            </div>
                        </> : null
                }
            </Modal>

            <style jsx>
                {`
                    .search-section {
                        min-height: 200px;
                        background-color: #fff;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        flex-direction: column;
                    }
                    .sub-input, .sub-btn {
                        height: 45px;
                        border: none;
                    }

                    .sub-input {
                        width: 250px;
                        background-color: #fff;
                        padding: 0 10px;
                        font-size: .9rem;
                        border-radius: 4px 0 0 4px;
                        border: 1px solid var(--primary-color);
                        color: inherit;
                    }

                    .sub-btn {
                        width: 150px;
                        color: #fff;
                        border-radius: 0 4px 4px 0;
                        background-color: var(--primary-color);
                        font-size: .9rem;
                    }

                    .sub-btn:disabled {
                        background-color: rgba(18, 104, 55, 0.8);
                    }

                    .div-form {
                        display: flex;
                        justify-content: center;
                        width: 100%;
                        padding: 0 20px;
                    }
                `}
            </style>
        </>
    )
}