import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { StorageService } from "../services/storage-service";
import { AppstoreFilled, WalletFilled, SoundFilled, ContactsFilled, QqCircleFilled, QuestionCircleFilled, InfoCircleFilled, WarningFilled } from "@ant-design/icons";
// import Link from "next/link";

export const NavBar = () => {
    const [name, setName] = useState('');
    const [profilePic, setProfilePic] = useState('');
    const [token, setToken] = useState('');

    const router = useRouter();

    useEffect(() => {
        const _token = StorageService.getToken();
        setToken(_token);
        getUser();
    }, []);

    const getUser = () => {
        let user = StorageService.getUserDetails();
        if (user) {
            setName(user.firstName + ' ' + user.lastName);
            setProfilePic(user.imageUrl);
        }
    }
    return (
        <>
            <nav id="sidebar" style={{ backgroundColor: '#271F1F' }}>
                {/* Sidebar Scroll Container */}
                <div id="sidebar-scroll">
                    {/* Sidebar Content */}
                    <div className="sidebar-content">
                        {/* Side Header */}
                        <div className="content-header content-header-fullrow px-15">
                            <div className="content-header-section text-center align-parent sidebar-mini-hidden">
                                <button type="button" className="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                                    <i className="fa fa-times text-danger" />
                                </button>
                                <div className="content-header-item">
                                    <a className="link-effect font-w700" href="/">
                                        <img src="images/logo.png" alt="logo" style={{ width: 100 }} />
                                    </a>
                                </div>
                            </div>
                        </div>

                        {
                            token
                                ? <div className="content-side content-side-full content-side-user px-10 align-parent" style={{ backgroundColor: 'transparent' }}>
                                    {/* Visible only in mini mode */}
                                    <div className="sidebar-mini-visible-b align-v animated fadeIn">
                                        <img
                                            className="img-avatar img-avatar32"
                                            src={profilePic || 'img/avatars/avatar15.jpg'}
                                            alt="profile"
                                        />
                                    </div>

                                    <div className="sidebar-mini-hidden-b text-center">
                                        <a className="img-link" href="/profile">
                                            <img
                                                className="img-avatar"
                                                src={profilePic || 'img/avatars/avatar15.jpg'}
                                                alt="profile"
                                            />
                                        </a>
                                        <ul className="list-inline mt-10">
                                            <li className="list-inline-item">
                                                <a className="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase" href="/profile">{name}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div> : null
                        }

                        <div className="content-side content-side-full">
                            <ul className="nav-main">
                                {
                                    token
                                        ? <>
                                            <li>
                                                <a href="/dashboard" className={router.pathname === '/dashboard' ? 'active-link' : ''}>
                                                    <AppstoreFilled />&nbsp;&nbsp;<span className="sidebar-mini-hide">Dashboard</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/wallet" className={router.pathname === '/wallet' ? 'active-link' : ''}>
                                                    <WalletFilled />&nbsp;&nbsp;<span className="sidebar-mini-hide">Wallet</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/marketplace" className={router.pathname === '/marketplace' ? 'active-link' : ''}>
                                                    <WalletFilled />&nbsp;&nbsp;<span className="sidebar-mini-hide">Marketplace</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/request" className={router.pathname === '/request' ? 'active-link' : ''}>
                                                    <SoundFilled />&nbsp;&nbsp;<span className="sidebar-mini-hide">Request</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/recipient" className={router.pathname === '/recipient' ? 'active-link' : ''}>
                                                    <ContactsFilled />&nbsp;&nbsp;<span className="sidebar-mini-hide">Recipients</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/profile" className={router.pathname === '/profile' ? 'active-link' : ''}>
                                                    <QqCircleFilled />&nbsp;&nbsp;<span className="sidebar-mini-hide">Profile (KYC)</span>
                                                </a>
                                            </li>
                                        </> : null
                                }
                                <br />
                                <br />
                                <br />
                                <li>
                                    <a href="/faq" className={router.pathname === '/faq' ? 'active-link' : ''}>
                                        <QuestionCircleFilled />&nbsp;&nbsp;<span className="sidebar-mini-hide">FAQ</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/terms" className={router.pathname === '/terms' ? 'active-link' : ''}>
                                        <InfoCircleFilled />&nbsp;&nbsp;<span className="sidebar-mini-hide">Terms</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/privacy" className={router.pathname === '/privacy' ? 'active-link' : ''}>
                                        <WarningFilled />&nbsp;&nbsp;<span className="sidebar-mini-hide">Privacy Policy</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </>
    )
}

const styles = {
    sideLinks: {
        color: 'red !important'
    }
}