export class RequestModel {
    bankId = 0;
    beneficiaryAccount = '';
    beneficiaryCurrency = '';
    beneficiaryName = '';
    rate = null;
    sortCode = '';
    swiftCode = '';
    valueAmount: number = null;
    valueCurrency = '';
    matchingTransId = null;
}