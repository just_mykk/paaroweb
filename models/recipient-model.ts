export class RecipientModel {
    accountCurrency = '';
    accountName = '';
    accountNumber = '';
    bankId = 0;
    bankName = '';
    bankSortCode = '';
    currencyId = 0;
    customerId = 0;
}