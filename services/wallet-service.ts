/** @format */

import { Endpoints } from './endpoints';
import BackendService from './backend-service';
import { TrueLayerService } from './truelayer-service';
import sharedService from './shared-service';
import Axios from 'axios';
import Swal from 'sweetalert2';

export class WalletService {
  static async getWallets(): Promise<Array<any>> {
    const res = await BackendService.get(Endpoints.getWallet);
    if (res.respCode === 0) return res.body;
    return [];
  }

  static async getWalletHistory(currency: string) {
    const res = await BackendService.get(Endpoints.getWalletHistory(currency));
  }

  static async cashout(body: any): Promise<{ error: boolean; message: string }> {
    const res = await BackendService.post(Endpoints.walletCashout, body);
    if (res.respCode === 0) return { error: false, message: res.respDesc };
    if (res.respCode === 99) return { error: true, message: res.respDesc };
  }

  static async getPaaroBanks(crncyCode: string): Promise<Array<any>> {
    const res = await BackendService.get(Endpoints.paaroBanks(crncyCode));
    if (res.respCode === 0) return res.body;
    return [];
  }

  static async fundWallet(body: any) {
    return await BackendService.post(Endpoints.fundWallet, body);
  }

  static async updateWalletFund(ref: string) {
    var body = {
      paymentMethod: 'CARD',
      paymentReference: ref,
      referenceNumber: ref,
    };
    return await BackendService.post(Endpoints.updateFunding, body);
  }

  static async trueLayerPayment(user: any, ref: string) {
    var res = await TrueLayerService.generateToken();
    sharedService.logger('true layer token', res);

    if (res.statusCode == 200) {
      //   return await initTrueLayerPayment(user, res.data['access_token'], ref);
    } else {
      return { status: 99, message: res.statusMessage };
    }
  }

  static async initTrueLayerPayment(user: any, token: string, ref: string) {
    var data = {
      //   "amount": fund.valueAmount * 100,
      //   "currency": fund.valueCurrency,
      beneficiary_name: user.firstName,
      beneficiary_reference: ref,
      beneficiary_sort_code: '087199',
      beneficiary_account_number: '93693487',
      remitter_reference: ref,
      redirect_uri: 'https://console.truelayer.com/redirect-page',
    };
    var res = await TrueLayerService.createPayment(token, data);

    if (res.statusCode == 200) {
      return {
        status: 0,
        reference: ref,
        link: res.data.results[0].auth_uri,
      };
    } else {
      return { status: 99, message: res.statusMessage };
    }
  }

  static async ravePayment(data: any, ref: string) {
    const body = {
      PBFPubKey: process.env.RAVE_KEY,
      txref: ref,
      ...data,
    };

    sharedService.logger('rave payment body', body);

    try {
      const res = await Axios.post(process.env.RAVE_URL, body);
      sharedService.logger('rave success', res);
      window.open(res.data.data.link, '_self');
    } catch (e) {
      sharedService.logger('rave error', e.response);
      Swal.fire({
        icon: 'error',
        title: 'Flutterwave Error',
        text: e.response.data.message
      });
    }
  }
}
