/** @format */

import Axios from 'axios';

export class TrueLayerService {
  static async generateToken() {
    var data = {
      client_id: process.env.TRUE_LAYER_CLIENT_ID,
      client_secret: process.env.TRUE_LAYER_CLIENT_SECRET,
      scope: 'payments',
      grant_type: 'client_credentials',
    };

    try {
      const res = await Axios.post('https://auth.truelayer-sandbox.com/connect/token', data);
      return res.data;
    } catch (error) {
      if (error.response) return error.response.data;
      return { respCode: 99, respDesc: error.message };
    }
  }

  static async createPayment(token: string, data: any) {
    try {
      const response = await Axios.post(
        'https://pay-api.truelayer-sandbox.com/single-immediate-payments',
        data,
        { headers: { authorization: `Bearer ${token}` } }
      );
      return response.data;
    } catch (e) {
      if (e.response) return e.response.data;
      return { respCode: 99, respDesc: e.message };
    }
  }
}
