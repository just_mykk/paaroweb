import { Endpoints } from './endpoints';
import BackendService from "./backend-service";

export class ProfileService {
    static async getKycList(crncy: string): Promise<any> {
        const res = await BackendService.get(Endpoints.kycByCurency(crncy));
        if (res.respCode === 0) return res.body;
        return {};
    }

    static async changePassword(body: any) {
        const res = await BackendService.post(Endpoints.changePassword, body);
        return BackendService.handleResponse(res);
    }

    static async uploadBill(body: any) {
        const res = await BackendService.post(Endpoints.uploadUtilityBill, body);
        return BackendService.handleResponse(res);
    }

    static async uploadPassport(body: any) {
        const res = await BackendService.post(Endpoints.uploadPassport, body);
        return BackendService.handleResponse(res);
    }

    static async uploadValidId(body: any) {
        const res = await BackendService.post(Endpoints.uploadValidId, body);
        return BackendService.handleResponse(res);
    }

    static async uploadBvn(body: any) {
        const res = await BackendService.post(Endpoints.uploadBvn, body);
        return BackendService.handleResponse(res);
    }

    static async uploadProfilePic(body: any) {
        const res = await BackendService.post(Endpoints.uploadProfilePic, body);
        return BackendService.handleResponse(res);
    }
}