import BackendService from "./backend-service";
import { Endpoints } from "./endpoints";

export class GlobalService {
    static addComma(value: number): string {
        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    static async getCountries(): Promise<Array<any>> {
        const res = await BackendService.get(Endpoints.getCountries);
        if (res.respCode === 99) return [];
        return res.body;
    }

    static async getReferenceTypes(): Promise<Array<any>> {
        const res = await BackendService.get(Endpoints.getReferenceTypes);
        if (res.respCode !== 99) return res.body;
        return [];
    }

    static async getBanksByCountry(countryCode: string) {
        const res = await BackendService.get(Endpoints.getBanksByCountry(countryCode));
        if (res.respCode === 0) return res.body;
        return [];
    }

    static async getCurrencies(): Promise<Array<any>> {
        const res = await BackendService.get(Endpoints.getCurrencies);
        if (res.respCode === 0) return res.body;
        return [];
    }

    static async getFaqs(): Promise<Array<any>> {
        const res = await BackendService.get(Endpoints.faqs);
        if (res.respCode === 0) return res.body;
        return [];
    }
}