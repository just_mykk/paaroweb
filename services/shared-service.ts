import { notification } from "antd";

const logger = (key: string, value: any) => {
    // console.log(key.toUpperCase(), value);
}

const showNotification = (type: string, message: string, title?: string) => {
    notification[type]({
        message: title,
        description: message
    });
}

const currSymbol = (curr: string): string => {
    switch(curr.toLowerCase()) {
        case 'gbp':
            return '£';
        case 'usd':
            return '$';
        default:
            return '₦';
    }
}

const currColor = (curr: string): string => {
    switch(curr.toLowerCase()) {
        case 'gbp':
            return 'linear-gradient(#2944B3, #090D67)';
        case 'usd':
            return 'linear-gradient(#B32963, #67092A)';
        default:
            return 'linear-gradient(#126637, #042816)';
    }
}

export default ({
    logger, showNotification,
    currSymbol, currColor
})