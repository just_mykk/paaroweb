import { Endpoints } from './endpoints';
import BackendService from "./backend-service";
import sharedService from './shared-service';

enum TranStatus {
    PENDING = 'PENDING',
    CANCELLED = 'CANCELLED',
    COMPLETED = 'SUCCESSFUL'
}

export class BeneficiaryModel {
    beneficiaryName = '';
    beneficiaryAccount = '';
    bankName = '';

    constructor(account?: string, name?: string, bank?: string) {
        this.beneficiaryAccount = account;
        this.beneficiaryName = name;
        this.bankName = bank;
    }
}

export class TransactionService {
    async pending(): Promise<[]> {
        const res = await BackendService.get(Endpoints.getTransactions(TranStatus.PENDING));
        if (res.respCode === 0) return res.body;
        return [];
    }

    async cancelled(): Promise<[]> {
        const res = await BackendService.get(Endpoints.getTransactions(TranStatus.CANCELLED));
        if (res.respCode === 0) return res.body;
        return [];
    }

    async completed(): Promise<[]> {
        const res = await BackendService.get(Endpoints.getTransactions(TranStatus.COMPLETED));
        if (res.respCode === 0) return res.body;
        return [];
    }

    async details(id: any, tranType: string): Promise<BeneficiaryModel> {
        let endpoint = '';
        const _tranType = tranType.toUpperCase();

        if (_tranType.toUpperCase() === 'FUND_WALLET') endpoint = Endpoints.fundDetails(id);
        if (_tranType.toUpperCase() === 'CASH_OUT') endpoint = Endpoints.cashoutDetails(id);
        if (_tranType.toUpperCase() === 'TRANSFER') endpoint = Endpoints.transferDetails(id);

        const res = await BackendService.get(endpoint);
        if (res.respCode === 0) {
            return new BeneficiaryModel(
                res.body.beneficiaryAccount,
                res.body.beneficiaryName,
                res.body.bankName
            )
        }
        return new BeneficiaryModel();
    }

    async searchBid(cid: string): Promise<any> {
        const res = await BackendService.get(Endpoints.bidDetails(cid));
        sharedService.logger('contract bid details', res);
        if (res.respCode === 0) return res.body;
        return null;
    }
}