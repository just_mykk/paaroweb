import { Endpoints } from './endpoints';
import BackendService from "./backend-service";
import sharedService from './shared-service';
import { RequestModel } from '../models/request-model';

export class RequestService {
    static async searchBids(body: any): Promise<Array<any>> {
        const res = await BackendService.post(Endpoints.searchBids, body);
        sharedService.logger('search bids', res);
        if (res.respCode === 0) return res.body;
        return [];
    }

    static getRate(list: Array<any>, fromCurr: string, toCurr: string): number {
        let rate = 0;
        for (var item of list) {
            if (item.type === fromCurr) {
                rate = item.averageRate;
                break;
            }
        }

        if (rate === null || rate === 0) {
            for (var item of list) {
                if (item.type === toCurr) {
                    rate = item.averageRate;
                    break;
                }
            }
        }

        return rate || 0;
    }

    static async getFee(body: any): Promise<number> {
        const res = await BackendService.post(Endpoints.processingFee, body);
        if (res.respCode === 0) {
            return res.body.charge;
        }
        return 0;
    }

    static async newRequest(body: RequestModel): Promise<{success: boolean, message: string}> {
        const res = await BackendService.post(Endpoints.transferBid, body);
        return { success: res.respCode === 0 ? true : false, message: res.respDesc };
    }

    static async openBids(): Promise<Array<any>> {
        const res = await BackendService.get(Endpoints.openBids);
        if (res.respCode === 0) return res.body;
        return [];
    }
}