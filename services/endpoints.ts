export class Endpoints {
    static login = '/security/token';
    static register = '/security/register';
    static resetPassword = '/security/resetPassword';
    static changePassword = '/security/changePassword';
    static uploadProfilePic = '/security/upload-pic';
    static getUser = '/security/me';

    static getTransactions = (status: string) => `/api/transaction?status=${status}`;
    static getPagedTransactions = (page: number, size: number, status: string) => `/api/transaction/paged?page=${page}&size=${size}&status=${status}`;

    static paaroBanks = (currencyCode: string) => `/api/paaro/banks?currencyCode=${currencyCode}`;

    static processingFee = '/processing-fee';
    static searchBids = '/api/bids/search';
    static transferBid = '/api/wallet/transfer-bid';

    static getRecipients = '/api/beneficiary';
    static validateNgnAccount = '/api/beneficiary/validate-ngn';
    static addRecipient = '/api/beneficiary/add';
    static deleteRecipient = (id: number) => `/api/beneficiary/${id}/delete`;
    static getRecipientByCurrency = (curr: string) => `/api/beneficiary/${curr}/currency`;

    static getWallet = '/api/wallet';
    static fundWallet = '/api/wallet/fund';
    static updateFunding = '/api/wallet/fund/update';
    static walletCashout = '/api/wallet/cash-out';
    static getWalletHistory = (currency: any) => `/api/wallet/${currency}/history?page=0&size=20`;
    static getWalletByCurrency = (currency: string) => `/api/wallet/${currency}`;

    static getBanks = '/banks';
    static getCountries = '/countries';
    static getCurrencies = '/currencies';
    static getReferenceTypes = '/reference-types';
    static getBanksByCountry = (countryCode: any) => `/banks/${countryCode}/country`;

    static uploadBvn = '/api/kyc/upload-bvn';
    static uploadPassport = '/api/kyc/upload-passport-photo';
    static uploadUtilityBill = '/api/kyc/upload-utility-bill';
    static uploadValidId = '/api/kyc/upload-valid-id';
    static kycByCurency = (currency: string) => `/api/kyc/${currency}`;

    static faqs = '/faqs';

    static marketplace = '/api/bids';

    static openBids = '/contract/bids';
    static pagedOpenBids = '/contract/bids/paged';
    static bidDetails = (cid: any) => `/contract/${cid}`;

    static cashoutDetails = (id: any) => `/api/cashout/${id}/detail`;
    static fundDetails = (id: any) => `/api/fund/${id}/detail`;
    static transferDetails = (id: any) => `/api/transfer/${id}/detail`;
}