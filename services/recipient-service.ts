import { Endpoints } from './endpoints';
import BackendService from "./backend-service"

export class RecipientService {
    static validateAccount = async (body: any) => {
        return await BackendService.post(Endpoints.validateNgnAccount, body);
    }

    static add = async (body): Promise<string> => {
        const res = await BackendService.post(Endpoints.addRecipient, body);
        return BackendService.handleResponse(res);
    }

    static delete = async (id: number): Promise<{ error: boolean, message: string }> => {
        const res = await BackendService.deleteRequest(Endpoints.deleteRecipient(id));

        if (res.respCode === 99) return { error: true, message: res.respDesc };

        if (res.respCode === 0) return { error: false, message: res.respDesc };
    }

    static get = async (): Promise<[]> => {
        const res = await BackendService.get(Endpoints.getRecipients);
        if (res.respCode === 99) return [];
        if (res.respCode === 0) return res.body;
    }

    static recipientByCurrency = async (crncy: string): Promise<[]> => {
        const res = await BackendService.get(Endpoints.getRecipientByCurrency(crncy));
        if (res.respCode === 99) return [];
        if (res.respCode === 0) return res.body;
    }
}