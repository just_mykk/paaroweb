import Axios, { AxiosResponse } from 'axios';
import { StorageService } from './storage-service';
import SharedService from './shared-service';
import Router from 'next/router';

const baseUrl = process.env.BASE_URL_LIVE;

let headers = { 'Content-Type': 'application/json' };

const setHeader = () => {
    const token = StorageService.getToken();
    if (token) {
        headers['Authorization'] = `Bearer ${token}`;
    }
    SharedService.logger('headers', headers);
}

const post = async (endpoint: string, body: any) => {
    setHeader();

    try {
        const res = await Axios.post(baseUrl + endpoint, body, { headers });
        SharedService.logger(endpoint, res);
        return validateResponse(res);
    } catch (error) {
        SharedService.logger(endpoint + ' error', error);
        if (error.response) return validateResponse(error.response);
        return { respCode: 99, respDesc: error.message };
    }
}

const get = async (endpoint: string) => {
    setHeader();

    try {
        const res = await Axios.get(baseUrl + endpoint, { headers });
        SharedService.logger(endpoint, res);
        return validateResponse(res);
    } catch (error) {
        SharedService.logger(endpoint + ' error', error.response);
        if (error.response) return validateResponse(error.response);
        return { respCode: 99, respDesc: error.message };
    }
}

const deleteRequest = async (endpoint: string) => {
    setHeader();

    try {
        const res = await Axios.delete(baseUrl + endpoint, { headers });
        SharedService.logger(endpoint, res);
        return validateResponse(res);
    } catch (error) {
        SharedService.logger(endpoint + ' error', error);
        if (error.response) return validateResponse(error.response);
        return { respCode: 99, respDesc: error.message };
    }
}

const validateResponse = (res: AxiosResponse) => {
    if (res.status == 401) {
        document.cookie = '@paaro=;';
        Router.push('/login');
        return;
    }
    return res.data;
}

const handleResponse = (response: any): string => {
    if (response.respCode === 99) return response.respDesc;
    return null
}

const BackendService = {
    post, get, handleResponse,
    deleteRequest
};

export default BackendService