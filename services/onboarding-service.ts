import BackendService from './backend-service';
import { Endpoints } from './endpoints';
import { StorageService } from './storage-service';

const userLogin = async (userName: string, password: string) => {
    const body = { userName, password, deviceType: 'WEB' };
    const res = await BackendService.post(Endpoints.login, body);
    if (res.respCode === 0) {
        StorageService.saveToken(res.token);
        await getUser();
    }
    return handleResponse(res);
}

const resetPassword = async (email: string) => {
    const res = await BackendService.post(Endpoints.resetPassword, {email});
    return handleResponse(res);
}

const registerUser = async (body: any): Promise<any> => {
    return await BackendService.post(Endpoints.register, body);
}

const getUser = async () => {
    const res = await BackendService.get(Endpoints.getUser);
    if (res.respCode === 0) {
        StorageService.saveUserId(res.body.userId);
        localStorage.removeItem('@det');
        StorageService.saveUserDetails(res.body.userDetails);
        return res.body.userDetails;
    }
    return null;
}

const handleResponse = (response: any): string => {
    if (response.respCode === 99) return response.respDesc;
    return null
}

const OnboardingService = {
    userLogin, resetPassword,
    registerUser, getUser
}

export default OnboardingService