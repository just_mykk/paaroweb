import { NextPageContext } from "next";

export class StorageService {
    static saveToken(token: string) {
        document.cookie = `@paaro=${token}`;
    }

    static getToken(context?: NextPageContext): string {
        return this.getSavedCookie('@paaro', context);
    }

    static saveUserId(userId: number) {
        localStorage.setItem('@paaroUser', userId.toString());
    }

    static getUserId(): number {
        const val = localStorage.getItem('@paaroUser');
        return val ? Number(val) : 0;
    }

    static saveUserDetails(val: any) {
        localStorage.setItem('@det', JSON.stringify(val));
    }

    static getUserDetails(): any {
        const val = localStorage.getItem('@det');
        return val ? JSON.parse(val) : null;
    }

    static saveFund(data: any) {
        localStorage.setItem('@fund', JSON.stringify(data));
    }

    static getSavedFund(): any {
        const val = localStorage.getItem('@fund');
        return val ? JSON.parse(val) : null;
    }

    static removeSavedFund() {
        localStorage.removeItem('@fund');
    }

    static getSavedCookie(cookieName: string, context?: NextPageContext): string {
        const name = cookieName + "=";
        const cookie = process.browser ? document.cookie : context.req.headers.cookie;
        // const cookie = document.cookie;
        const decodedCookie = decodeURIComponent(cookie);
        const ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return '';
    }
}