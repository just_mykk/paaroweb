const express = require('express');
const next = require('next');

const port = process.env.PORT || 3300;
const dev = process.env.NODE_ENV !== 'production';
const prod = process.env.NODE_ENV === 'production';
const app = next({prod});
const handle = app.getRequestHandler();

const hostname = '161.35.78.88';

app.prepare().then(() => {
    const server = express();

    server.all('*', (req, res) => {
        return handle(req, res);
    });

    server.listen(port, (err) => {
        if (err) throw err;
        console.log(`App running on port ${port}`);
    });
});