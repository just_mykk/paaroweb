import React from 'react';
import Router from 'next/router';
import { StorageService } from '../services/storage-service';
import jtwDecode from 'jwt-decode';
import { NextPageContext } from 'next';
import sharedService from '../services/shared-service';

export const withoutAuth = (C) => {
    return class extends React.Component<{ showPage: boolean }> {
        static async getInitialProps(ctx: NextPageContext) {
            sharedService.logger('ctx', ctx);
            const token = StorageService.getToken(ctx);

            if (token && ctx.req) {
                ctx.res.writeHead(302, {Location: '/dashboard'});
                ctx.res.end();
            };

            if (token && !ctx.req) Router.push('/dashboard');

            return { me: '' };
        }

        render() {
            return <C {...this.props} />;
        }
    }
}
